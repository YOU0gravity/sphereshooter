﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// ゲーム起動時にロゴをアニメーションさせるためのクラス
/// </summary>
public class LogoController : MonoBehaviour {
	[SerializeField] private GameObject[] O_ShapedObjects;
	[SerializeField] private GameObject[] MeteoObjects;
	[SerializeField] private GameObject[] MeteoModels;
	[SerializeField] private GameObject FirstRowParent;
	[SerializeField] private GameObject SecondRowParent;
	[SerializeField] private GameObject StartMenuParent;

	private float LerpAndSlerpTimeRecord;
	
	//Lerping strings
	private bool IsLerpingFirstRow;
	private bool IsLerpingSecondRow;
	private Vector3 FirstRowStartPosition;
	private Vector3 SecondRowStartPosition;
	private Vector3 EndPosition = new Vector3(-6, 0, 0);


	//Slerping logo
	private bool IsSlerping;
	private Quaternion StartRotation;
	private Quaternion EndRotation = Quaternion.Euler(-7, 0, 45);


	//Scaling logo
	private bool IsScaling;
	private bool IsEnlarging;
	private float ParentScale = 1;


	//Lerping Meteo
	private bool IsLerpingFirstMeteo;
	private bool IsLerpingSecondMeteo;
	private Vector3 FirstMeteoStartPos;
	private Vector3 FirstMeteoEndPos = new Vector3(4, 0, 1);
	private Vector3 MeteoOffset = new Vector3(2, 0, 0);


	//Lerping Logo
	private bool IsLerpingLogo;
	private Vector3 LogoStartPos;
	private Vector3 LogoEndPos = new Vector3(0, 0, 22);


	// Use this for initialization
	void Start () {
		this.FirstRowStartPosition = this.FirstRowParent.transform.localPosition;
		this.SecondRowStartPosition = this.SecondRowParent.transform.localPosition;

		this.IsLerpingFirstRow = true;
		this.LerpAndSlerpTimeRecord = Time.timeSinceLevelLoad;

		this.StartRotation = transform.localRotation;

		this.FirstMeteoStartPos = this.MeteoObjects[0].transform.localPosition;

		this.LogoStartPos = transform.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
		for(int i=0; i<2; i++){
			if(this.MeteoObjects[i].activeSelf)
				this.MeteoModels[i].transform.Rotate(Vector3.one * 45 * Time.deltaTime);
		}

		//"SPHERE"と"SHOOTER"という文字列がそれぞれ左右の画面外からスライドして、中央で止まる
		if(this.IsLerpingFirstRow){
			var diff = Time.timeSinceLevelLoad - this.LerpAndSlerpTimeRecord;
			var time = 0.5f;
			if (diff > time) {
				this.FirstRowParent.transform.localPosition = this.EndPosition;
				this.IsLerpingFirstRow = false;

				this.IsLerpingSecondRow = true;
				this.LerpAndSlerpTimeRecord = Time.timeSinceLevelLoad;
				// EffectManager.Instance.ApplyEffect("Roulette", Vector3.zero);
			}
			var rate = diff / time;
			this.FirstRowParent.transform.localPosition = Vector3.Lerp (this.FirstRowStartPosition, this.EndPosition, rate);
		}
		if(this.IsLerpingSecondRow){
			var diff = Time.timeSinceLevelLoad - this.LerpAndSlerpTimeRecord;
			var time = 0.5f;
			if (diff > time) {
				this.SecondRowParent.transform.localPosition = this.EndPosition + new Vector3(0, 0, -2);
				this.IsLerpingSecondRow = false;

				this.IsSlerping = true;
				this.LerpAndSlerpTimeRecord = Time.timeSinceLevelLoad;
				// EffectManager.Instance.ApplyEffect("Roulette", Vector3.zero);
			}
			var rate = diff / time;
			this.SecondRowParent.transform.localPosition = Vector3.Lerp (this.SecondRowStartPosition, this.EndPosition + new Vector3(0, 0, -2), rate);
		}

		//スライドが終了すると、ロゴが若干傾いて立体的になる
		if(this.IsSlerping){
			var diff = Time.timeSinceLevelLoad - this.LerpAndSlerpTimeRecord;
			if (diff > 1) {
				transform.rotation = this.EndRotation;
				this.IsSlerping = false;

				this.IsScaling = true;
				this.IsEnlarging = true;
				// EffectManager.Instance.ApplyEffect("DimensionChange", Vector3.zero);
			}

			var rate = diff / 1;
			transform.rotation = Quaternion.Slerp(this.StartRotation, this.EndRotation, rate);
		}

		//ロゴが大きくなっていく（最後の方で、反動により少し小さくなる）
		if(this.IsScaling){
			if(this.IsEnlarging){
				this.ParentScale += Time.deltaTime * 10;
				if(this.ParentScale > 2f)
					this.IsEnlarging = false;

			}else{
				this.ParentScale -= Time.deltaTime * 5;
				if(this.ParentScale < 1.5f){
					this.IsScaling = false;
					this.ParentScale = 1.5f;
					
					this.IsLerpingFirstMeteo = true;
					this.LerpAndSlerpTimeRecord = Time.timeSinceLevelLoad;
					this.MeteoObjects[0].SetActive(true);
				}
			}
			transform.localScale = Vector3.one * this.ParentScale;
		}

		//隕石が飛んできて、"O"という文字を破壊する
		if(this.IsLerpingFirstMeteo){
			var diff = Time.timeSinceLevelLoad - this.LerpAndSlerpTimeRecord;
			var time = 0.3f;
			if (diff > time) {
			// 	EffectManager.Instance.ApplyEffect("Explosion", this.O_ShapedObjects[0].transform.position + new Vector3(0,0,1));
				EffectManager.Instance.ApplyEffect("ShakeS");
				Destroy(this.O_ShapedObjects[0]);

				this.MeteoObjects[0].transform.localPosition = this.FirstMeteoEndPos;
				this.IsLerpingFirstMeteo = false;

				this.IsLerpingSecondMeteo = true;
				this.LerpAndSlerpTimeRecord = Time.timeSinceLevelLoad;
				this.MeteoObjects[1].SetActive(true);
			}
			var rate = diff / time;
			this.MeteoObjects[0].transform.localPosition = Vector3.Lerp (this.FirstMeteoStartPos, this.FirstMeteoEndPos, rate);
		}
		if(this.IsLerpingSecondMeteo){
			var diff = Time.timeSinceLevelLoad - this.LerpAndSlerpTimeRecord;
			var time = 0.3f;
			if (diff > time) {
				// EffectManager.Instance.ApplyEffect("Explosion", this.O_ShapedObjects[1].transform.position + new Vector3(0,0,1));
				EffectManager.Instance.ApplyEffect("ShakeS");
				Destroy(this.O_ShapedObjects[1]);

				this.MeteoObjects[1].transform.localPosition = this.FirstMeteoEndPos + this.MeteoOffset;
				this.IsLerpingSecondMeteo = false;

				this.IsLerpingLogo = true;
				this.LerpAndSlerpTimeRecord = Time.timeSinceLevelLoad;
			}
			var rate = diff / time;
			this.MeteoObjects[1].transform.localPosition = Vector3.Lerp (this.FirstMeteoStartPos + this.MeteoOffset, this.FirstMeteoEndPos + this.MeteoOffset, rate);
		}

		//最後に、ロゴが上へ移動する
		if(this.IsLerpingLogo){
			var diff = Time.timeSinceLevelLoad - this.LerpAndSlerpTimeRecord;
			var time = 1f;
			if (diff > time) {
				transform.localPosition = this.LogoEndPos;
				this.IsLerpingLogo = false;
				
				this.StartMenuParent.SetActive(true);

				GameManager.Instance.IsLogoAnimationEnded = true;
			}
			var rate = diff / time;
			transform.localPosition = Vector3.Lerp (this.LogoStartPos, this.LogoEndPos, rate);
		}

		if(Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space)){
			this.StopAnimationImmediately();
		}

	}

	private void StopAnimationImmediately(){
		this.IsLerpingFirstRow = false;
		this.IsLerpingSecondRow = false;
		this.IsSlerping = false;
		this.IsScaling = false;
		this.IsLerpingFirstMeteo = false;
		this.IsLerpingSecondMeteo = false;
		this.IsLerpingLogo = false;
		GameManager.Instance.IsLogoAnimationEnded = true;

		this.FirstRowParent.transform.localPosition = this.EndPosition;
		this.SecondRowParent.transform.localPosition = this.EndPosition + new Vector3(0, 0, -2);
		transform.rotation = this.EndRotation;
		transform.localScale = Vector3.one * 1.5f;
		Destroy(this.O_ShapedObjects[0]);
		Destroy(this.O_ShapedObjects[1]);
		this.MeteoObjects[0].SetActive(true);
		this.MeteoObjects[1].SetActive(true);
		this.MeteoObjects[0].transform.localPosition = this.FirstMeteoEndPos;
		this.MeteoObjects[1].transform.localPosition = this.FirstMeteoEndPos + this.MeteoOffset;
		transform.localPosition = this.LogoEndPos;
		this.StartMenuParent.SetActive(true);
	}


}
