﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 背景にある小物（プレイヤーが触ることができないもの）をコントロールします
/// Dimensionが変更されたとき、それに合わせて位置を移動します
/// </summary>
public class BackgroundObjectController : MonoBehaviour {
	private bool IsLerping;
	private Vector3 OriginPos;
	private Vector3 StartPos;
	private Vector3 EndPos;
	private float StartTime;

	void Start(){
		this.OriginPos = transform.localPosition;
		this.StartPos = this.OriginPos;
		this.ChangeDimensions(GameManager.DimensionNumber);
	}

	void Update(){
		if(this.IsLerping){
			var diff = Time.timeSinceLevelLoad - this.StartTime;
			if (diff > 1) {
				transform.position = this.EndPos;
				this.IsLerping = false;
			}
			var rate = diff / 1;
			transform.localPosition = Vector3.Lerp (this.StartPos, this.EndPos, rate);
		}
	}
	public void ChangeDimensions(int dimensionNumber){
		this.EndPos = this.OriginPos;
		if(dimensionNumber == 2)
			this.EndPos.y = 0;

		if(dimensionNumber == 3)
			this.EndPos.x = 0;
		
		this.StartPos = transform.localPosition;
		this.IsLerping = true;
		this.StartTime = Time.timeSinceLevelLoad;
	}
}
