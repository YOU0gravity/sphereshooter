using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// スタートメニューのボタンを管理する
/// </summary>
public class StartMenuController : MonoBehaviour {
    [SerializeField] private GameObject FourButtonsContainer;
    [SerializeField] private GameObject BigRouletteButton;
    private bool IsBigRouletteShowing;

    void Start(){
        this.FourButtonsContainer.SetActive(true);
        this.BigRouletteButton.SetActive(false);
        this.IsBigRouletteShowing = false;
    }
    public void Roulette(){
        this.IsBigRouletteShowing = !this.IsBigRouletteShowing;
        this.FourButtonsContainer.SetActive(!this.IsBigRouletteShowing);
        this.BigRouletteButton.SetActive(this.IsBigRouletteShowing);
        GameManager.Instance._selectingCharacterController.ActivateRoulette();
    }
    public void SelectCharacter(bool isRight){
        GameManager.Instance.ChooseCharacter(isRight);
    }
    public void StartGame(){
        GameManager.Instance.StartStage();
    }
    public void HideButtons(){
        Destroy(gameObject);
    }
}
