﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 障害物に関する情報を持ちます
/// </summary>
public class EnemyController : MonoBehaviour {
	public int DropMoneyAmount;
	[SerializeField] private bool IsRotating = false;
	[SerializeField] private GameObject EnemyModel;

	[SerializeField] private int DefeatableDimension = 0;

	[SerializeField] private float EnemyHP;

	private Quaternion BeforeRotation;
	private bool IsSlerping;


	private float StartTime;
	private Vector3 EndPos;
	private Vector3 StartPos;
	private Vector3 OriginPos;

	// Use this for initialization
	void Start () {
		this.OriginPos = transform.position;
		this.EndPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if(GameManager._smoothCameraMover.enabled){
			var diff = Time.timeSinceLevelLoad - this.StartTime;
			if (diff > 1) {
				transform.position = this.EndPos;
			}
			var rate = diff / 1;
			transform.position = Vector3.Lerp (this.StartPos, this.EndPos, rate);

			this.EnemyModel.transform.rotation = Quaternion.Slerp(this.EnemyModel.transform.rotation, Quaternion.identity, Time.deltaTime * 2);

		}else{
			if(this.IsSlerping){
				this.IsSlerping = false;
				this.EnemyModel.transform.rotation = Quaternion.identity;
			}

			if(this.IsRotating){
				switch(GameManager.DimensionNumber){
					case 0:
						this.EnemyModel.transform.Rotate(new Vector3(45,45,0) * Time.deltaTime);
						break;
					case 2:
						this.EnemyModel.transform.Rotate(new Vector3(0,90,0) * Time.deltaTime);
						break;
					case 3:
						this.EnemyModel.transform.Rotate(new Vector3(90,0,0) * Time.deltaTime);
						break;
				}
			}
		}
	}

	public void ChangeDimensions(int dimensionNumber){
		this.BeforeRotation = this.EnemyModel.transform.rotation;
		this.IsSlerping = true;

		this.StartTime = Time.timeSinceLevelLoad;
		this.StartPos = transform.position;
		switch(dimensionNumber){
			case 0:
				this.EndPos = this.OriginPos;
				// transform.rotation = Quaternion.identity;
				break;
			case 1: case 3:
				this.EndPos = new Vector3(0, this.OriginPos.y, this.OriginPos.z);
				break;
			case 2: case 4:
				this.EndPos = new Vector3(this.OriginPos.x, 0, this.OriginPos.z);
				// transform.rotation = Quaternion.Euler(new Vector3(90, 0, 0));
				break;
		}
	}

	private bool Damage(float damageAmount){
		this.EnemyHP -= damageAmount;
		if(this.EnemyHP <= 0){
			return true;
		}else{
			return false;
		}
	}

	public void OnTriggerEnter(Collider other){
		if(other.gameObject.tag == "Player"){
			GameManager.Instance.Collide();
		}

		if(other.gameObject.tag == "Bullet"){

			if(this.DefeatableDimension == 0){
				//次元: 全て
				//弱点: 0の場合

				//破壊: 可能
				//報酬: 有
				float damage = other.gameObject.GetComponent<BulletController>().WeaponAttack;
				bool isDestroyed = this.Damage(damage);
				if(isDestroyed){
					GameManager.Instance.DefeatEnemy(gameObject, false);
				}else{
					StartCoroutine("ShineOneShot");
				}
				other.gameObject.SendMessage("Collide");

			}else if(this.DefeatableDimension == -1){
				//次元: 全て
				//弱点: -1の場合

				//破壊: 不能
				//報酬: 無し
				Vector3 pos = transform.position;
				switch(GameManager.DimensionNumber){
					case 2:
						pos += new Vector3(0, 6, -1);
						break;
					case 3:
						pos += new Vector3(6, 1f, 0);
						break;
				}
				EffectManager.Instance.ApplyEffect("Protection", pos);
				other.gameObject.SendMessage("Collide");

			}else{
				if(GameManager.DimensionNumber == this.DefeatableDimension){
					//次元: 0以外
					//弱点: 次元と一致している場合

					//破壊: 可能
					//報酬: 有
					float damage = other.gameObject.GetComponent<BulletController>().WeaponAttack;
					bool isDestroyed = this.Damage(damage);
					if(isDestroyed){
						GameManager.Instance.DefeatEnemy(gameObject, false);
					}else{
						StartCoroutine("ShineOneShot");
					}
					other.gameObject.SendMessage("Collide");

				}else{
					if(GameManager.DimensionNumber == 0){
						//次元: 0
						//弱点: 次元と一致していない場合

						//破壊: 不可能
						//報酬: 無
						Vector3 pos = transform.position;
						switch(GameManager.DimensionNumber){
							case 2:
								pos += new Vector3(0, 6, -1);
								break;
							case 3:
								pos += new Vector3(6, 1f, 0);
								break;
						}
						EffectManager.Instance.ApplyEffect("Protection", pos);
						other.gameObject.SendMessage("Collide");

					}else{
						//次元: 0以外
						//弱点: 次元と一致していない場合

						//破壊: 不可能
						//報酬: 無
						Vector3 pos = transform.position;
						switch(GameManager.DimensionNumber){
							case 2:
								pos += new Vector3(0, 6, -1);
								break;
							case 3:
								pos += new Vector3(6, 1f, 0);
								break;
						}
						EffectManager.Instance.ApplyEffect("Protection", pos);
						other.gameObject.SendMessage("Collide");
					}
				}

			}

		}
	}
}
