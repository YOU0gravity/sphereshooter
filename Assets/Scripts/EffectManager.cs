﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// サウンドとパーティクルなどのエフェクトを管理するクラス
/// </summary>
public class EffectManager : MonoBehaviour {
	private AudioSource[] AudioSources;//0: BGM, 1: Sounds(except for coin sound) 2: Sounds(coin sound)
	private AudioClip[] Sounds;// 0: Explosion, 1: Coin, 2: Invincible, 3: Shoot, 4: Protection, 5: DamageForPlayer, 6:Unlocked
	private GameObject[] VisualEffects;//0: Explosion, 1: PickUp, 2: CoinSpread
	[SerializeField] private ParticleSystem ProtectionParticle;
	private float LastProtectionOccuredTime;
	private float LastExplosionOccuredTime;
	private bool IsLightWeightMode = false;//TODO: Configファイルから参照するようにする

	[SerializeField] private SmoothCameraMover _smoothCameraMover;
	[SerializeField] private CameraShaker _cameraShaker;


	//Singleton Instance
	private static EffectManager instance;
	public static EffectManager Instance {
		private set { instance = value; }
		get{ return instance; }
	}

	void Awake(){
		if(instance == null){
			instance= this;
		}else{
			Destroy(gameObject);
		}
	}

	void Start(){
		//SoundEffect
		Sounds = new AudioClip[10];
		for(int i=0; i<Sounds.Length; i++){
			Sounds[i] = (AudioClip)Resources.Load("Sounds/Sound" + i.ToString());
		}

		AudioSources = new AudioSource[4];
		for(int i=0; i<4; i++){
			AudioSources[i] = gameObject.AddComponent<AudioSource>();
		}
		AudioSources[0].clip = (AudioClip)Resources.Load("Sounds/BGM0");
		AudioSources[0].loop = true;
		AudioSources[0].volume = 1.0f;
		AudioSources[0].pitch = 0.8f;

		AudioSources[1].volume = 0.5f;
		//Coin
		AudioSources[2].clip = Sounds[1];
		AudioSources[2].volume = 0.8f;

		//Shoot
		AudioSources[3].clip = Sounds[3];
		AudioSources[3].volume = 0.2f;

		//VisualEffect
		if(!IsLightWeightMode){
			VisualEffects = new GameObject[3];
			for(int i=0; i<3; i++){
				VisualEffects[i] = Resources.Load("Effects/" + i.ToString()) as GameObject;
			}
		}
	}
	public void PlayBGM(bool play){
		if(play){
			AudioSources[0].Play();
		}else{
			AudioSources[0].Stop();
		}
	}
	public void SetPitch(float pitch){
		AudioSources[0].pitch = pitch;
	}

	public void ApplyEffect(string name){
		ApplyEffect(name, Vector3.zero);
	}

	public void ApplyEffect(string name, Vector3 pos){
		switch(name){
			case "Explosion":
				if(Time.timeSinceLevelLoad - LastExplosionOccuredTime > 0.1f){
					AudioSources[1].PlayOneShot(Sounds[0]);
					
					if(!IsLightWeightMode && pos != Vector3.zero){
						GameObject effect = Instantiate(VisualEffects[0], pos, Quaternion.identity) as GameObject;
						Destroy(effect, 2f);
					}

				}
				LastExplosionOccuredTime = Time.timeSinceLevelLoad;
				
				break;


			case "CoinPickUp":
				AudioSources[2].Stop();
				AudioSources[2].time = 0.08f;
				AudioSources[2].Play();
				
				if(!IsLightWeightMode && pos != Vector3.zero){
					GameObject effect = Instantiate(VisualEffects[1], pos, Quaternion.identity) as GameObject;
					Destroy(effect, 2f);
				}
				break;
			
			case "CoinSpread":
				if(!IsLightWeightMode && pos != Vector3.zero){
					GameObject effect = Instantiate(VisualEffects[2], pos, Quaternion.identity) as GameObject;
					Destroy(effect, 2f);
				}
				break;

			case "DimensionChange":
				AudioSources[1].PlayOneShot(Sounds[2]);
				break;
			
			case "Protection":
				if(Time.timeSinceLevelLoad - LastProtectionOccuredTime > 0.3f){
					AudioSources[1].PlayOneShot(Sounds[4]);
					if(pos != Vector3.zero){
						ProtectionParticle.transform.position = pos;
						ProtectionParticle.Emit(3);
					}
					LastProtectionOccuredTime = Time.timeSinceLevelLoad;
				}
				break;

			case "Shoot":
				AudioSources[3].Play();
				break;
			
			case "Unlocked":
				AudioSources[1].PlayOneShot(Sounds[6]);
				break;

			case "Roulette":
				AudioSources[1].PlayOneShot(Sounds[7]);
				break;

			case "Damage":
				AudioSources[1].PlayOneShot(Sounds[5]);
				break;

			case "Button":
				AudioSources[1].PlayOneShot(Sounds[8]);
				break;

			case "Key":
				AudioSources[1].PlayOneShot(Sounds[9]);
				break;
			
			case "ShakeS":
				if(!_smoothCameraMover.enabled)
					_cameraShaker.Shake(0.01f);
				break;
			case "ShakeM":
				if(!_smoothCameraMover.enabled)
					_cameraShaker.Shake(0.03f);
				break;
			case "ShakeL":
				if(!_smoothCameraMover.enabled)
					_cameraShaker.Shake(0.05f);
				break;
		}
	}
}
