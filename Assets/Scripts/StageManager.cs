﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// ゲームプレイのステージを作成・コントロールするためのクラス
/// </summary>
public class StageManager : MonoBehaviour {
	private static StageManager _stageManagerr;
	public static StageManager Instance {
		private set { _stageManagerr = value; }
		get{ return _stageManagerr; }
	}

	private string StageName; //Space, Sea, Sky
	[SerializeField] private GameObject SpaceParticle;
	[SerializeField] private GameObject SeaParticle;
	[SerializeField] private GameObject SkySun;
	private int LastInstantiatedBackgroundObjectNumber;
	
	private GameObject[] EnemyPrefabs = new GameObject[100];
	private Dictionary<int, string> EnemyNameDic = new Dictionary<int, string>(){
		{1, "Sphere"},{2, "CylinderV"},{3, "CylinderH"},{99, "SpecialSphere"}
	};
	private GameObject MoneyPrefab;

	private float StageStartPosZ;
	public static readonly float AreaLength = 5f;

	private List<GameObject> RoadsList = new List<GameObject>();
	private int[,] RoadsInfo;
	private int FloorCount;

	[System.NonSerialized] public int CurrentStageLevel;

	void Awake(){
		if(Instance == null){
			Instance = this;
		}else{
			Destroy(gameObject);
		}
	}
	void Start()
	{
		MoneyPrefab = Resources.Load("Money") as GameObject;
		RoadsInfo = StageData.Instance.RoadsInfo;
	}

	public void Initialize(int stageNumber, float stageStartPosZ, bool isTutorial = false){
		FloorCount = 0;
		switch(stageNumber){
			case 0:
				StageName = "Space";
				break;
			case 1:
				StageName = "Sea";
				break;
			case 2:
				StageName = "Sky";
				break;
		}

		if(isTutorial)
			StageData.Instance.CreateRoadsInfo();

		string colorCode = "";
		switch(StageName){
			case "Space":
				colorCode = "#14141400";
				SpaceParticle.SetActive(true);
				break;
			case "Sea":
				colorCode = "#2FB8B600";
				SeaParticle.SetActive(true);
				break;
			case "Sky":
				colorCode = "#19B2EEE6";
				SkySun.SetActive(true);
				break;
		}
		Color changingColor = default(Color);
		if(ColorUtility.TryParseHtmlString(colorCode, out changingColor)){
			Camera.main.backgroundColor = changingColor;
		}else{
			Debug.Log("Error: 背景の色を変えることができなかった");
		}
		

		this.StageStartPosZ = stageStartPosZ;

		if(!isTutorial){
			for(int i=0; i<4; i++)
				CreateNextRoad();
		}
	}

	
	//ステージデータを読み取って、ここで作成する
	public void CreateNextRoad(){	
		if(FloorCount < RoadsInfo.GetLength(0)){
			Vector3 offsetPos = new Vector3(0, 0, StageStartPosZ + CurrentStageLevel * 15);
			GameObject oneRoad = Instantiate(Resources.Load("Stages/StageParent"), offsetPos, Quaternion.identity) as GameObject;
			oneRoad.name = "Road" + CurrentStageLevel.ToString();
			RoadsList.Add(oneRoad);

			int insCount = 0;
			for(int z=0; z<3; z++){
				for(int y=0; y<3; y++){
					for(int x=0; x<3; x++){
						Vector3 insPos = new Vector3(AreaLength*(1-x), AreaLength*(1-y), AreaLength*(1-z));
						int insNum = RoadsInfo[FloorCount, insCount];

						GameObject insItem = InstantiateItems(insNum, offsetPos + insPos);
						if(insItem != null)
							insItem.transform.parent = oneRoad.transform;

						GameObject insEnemy = InstantiateEnemies(insNum, offsetPos + insPos);
						if(insEnemy != null)
							insEnemy.transform.parent = oneRoad.transform;

						insCount++;
					}
				}
			}

			//Background objects
			int insProbability = 0;
			int maxInsKind = 0;
			float[] insRange = new float[]{8f, 15f, 9f, 22f};
			int insPerBox = 1;
			switch(StageName){
				case "Space":
					insProbability = 10;
					maxInsKind = 3;
					insRange = new float[]{15f, 20f, 13f, 20f};
					break;
				case "Sea":
					insProbability = 40;
					maxInsKind = 1;
					insPerBox = 10;
					break;
				case "Sky":
					insProbability = 50;
					maxInsKind = 3;
					insPerBox = 5;
					break;
			}

			int insBackgroundObjectCount = 0;
			for(int i=0; i<10; i++){
				if(Random.Range(0, 100) < insProbability){
					float x = Random.Range(insRange[0], insRange[1]);
					if(Random.Range(0,2) == 0){
						x = -x;
					}
					float y = Random.Range(insRange[2], insRange[3]);
					if(Random.Range(0,2) == 0){
						y = -y;
					}
					float z = Random.Range(0f, AreaLength);
					
					Vector3 insPos = new Vector3(x,y,z) + offsetPos;
					int insKind = -1;
					while(insKind == -1){
						int rand = Random.Range(0, maxInsKind);
						if(rand != LastInstantiatedBackgroundObjectNumber)
							insKind = rand;
						
						if(maxInsKind == 1)
							insKind = 0;
					}
					GameObject obj = Instantiate(Resources.Load("Stages/" + StageName + "/" + insKind), insPos, Quaternion.identity) as GameObject;
					obj.transform.parent = oneRoad.transform;
					obj.name = "Accessory";

					insBackgroundObjectCount++;
					LastInstantiatedBackgroundObjectNumber = insKind;
				}
				if(insBackgroundObjectCount >= insPerBox)
					break;
			}
			
			if(RoadsList.Count > 6){
				GameObject destroyingBox = RoadsList.First();
				RoadsList.RemoveAt(0);
				Destroy(destroyingBox, 3f);
			}

		}else{
			//読み込んだマップをすべて出現させた場合、最初から
			FloorCount = -1;
		}

		FloorCount++;
		CurrentStageLevel++;

		if(FloorCount == 0){
			CurrentStageLevel--;
			CreateNextRoad();
		}
	}

	private GameObject InstantiateItems(int insNum, Vector3 insPos){
		GameObject insObj = null;
		//アイテムが存在するとき
		if(insNum >= 100){
			int itemNumber = insNum / 100;
			switch(itemNumber){
				case 1: //コイン
					insObj = Instantiate(MoneyPrefab, insPos, Quaternion.Euler(0,180,0)) as GameObject;
					break;
			}
		}
		return insObj;
	}

	private GameObject InstantiateEnemies(int insNum, Vector3 insPos){
		GameObject insObj = null;
		insNum = insNum % 100;
		//敵が存在する(1~99)
		if(insNum > 0){
			//最初に読み込まれていない敵の場合、追加で読み込む
			if(EnemyPrefabs[insNum] == null)
				EnemyPrefabs[insNum] = Resources.Load("Stages/" + StageName + "/" + EnemyNameDic[insNum]) as GameObject;
			
			insObj = Instantiate(EnemyPrefabs[insNum], insPos, Quaternion.identity) as GameObject;
		}
		return insObj;
	}
}
