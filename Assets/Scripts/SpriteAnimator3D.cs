﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// スプライトアニメーションを3Dモデルで行うためのスクリプト
/// </summary>
public class SpriteAnimator3D : MonoBehaviour {
	[SerializeField] private GameObject[] BulletModels;
	[SerializeField] private float ChangeOffset;
	private float TimeRecord;
	private int ModelNumber;

	void Start(){
		for(int i=0; i<this.BulletModels.Length; i++){
			this.BulletModels[i].SetActive(false);
		}
		this.BulletModels[0].SetActive(true);
	}

	void Update () {
		this.TimeRecord += Time.deltaTime;
		if(this.TimeRecord > this.ChangeOffset){
			this.BulletModels[this.ModelNumber].SetActive(false);

			this.ModelNumber++;
			if(this.ModelNumber >= this.BulletModels.Length)
				this.ModelNumber = 0;
			
			this.BulletModels[this.ModelNumber].SetActive(true);
			this.TimeRecord = 0;
		}
	}
}
