﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// キャラクターを選択・変更するためのクラス
/// </summary>
public class SelectingCharacterController : MonoBehaviour {
	private GameObject[] Models;
	private float CharaOffset = 8;
	private int RotatingCharaID;

	private float RotateSpeedCoef = 1;

	private Vector3 StartLocalPos;
	private Vector3 EndLocalPos;
	private float StartTime;
	private bool IsLerping;
	private float EndTime = 0.5f;
	[SerializeField] private Material UnlockedShadowMat;
	public Text CharaNameText;
	[SerializeField] private GameObject ModelContainer;

	private bool IsRouletteSpining;
	public bool IsProcessingRoulette;
	private int StopID;


	//3DMoel
	private float OriginScaleUnit = 50;
	private float EnlargingScaleUnit;
	private float MaxScaleUnit = 150;
	private bool IsAppearing;
	private bool IsEnlarging;


	[SerializeField] private GameObject FourButtonsContainer;
	[SerializeField] private GameObject[] FourButton;
    [SerializeField] private GameObject BigRouletteButton;
	[SerializeField] private GameObject BackButton;
	[SerializeField] private GameObject TitleLogo;
	public GameObject TutorialButton;
    private bool IsBigRouletteShowing;

	private bool IsButtonEnlarging;
	private float ButtonTransformTime = 1;


	private bool IsDuringStopPerformance;


	// Use this for initialization
	void Start () {
		this.Models = new GameObject[GameManager.TotalCharacterNumber];
		for(int i=0; i<GameManager.TotalCharacterNumber; i++){
			int prefabNumber = GameManager.Instance.CharacterInfoArray[i].PlayerID;
			this.Models[i] = Instantiate(Resources.Load("PlayerModels/" + prefabNumber)) as GameObject;
			this.Models[i].transform.parent = this.ModelContainer.transform;
			this.Models[i].transform.localPosition = new Vector3(this.CharaOffset * i, 0, 0);
			this.Models[i].transform.localScale = new Vector3(50, 50, 50);
			this.Models[i].transform.localRotation = Quaternion.Euler(new Vector3(60, 0, 0));

			if(GameManager.Instance._characterLockManager.UnlockedNumber[i] == 0){
				MeshRenderer[] meshs = this.Models[i].GetComponentsInChildren<MeshRenderer>();
				for(int m=0; m<meshs.Length; m++)
					meshs[m].material = this.UnlockedShadowMat;
			}
			
		}

		this.MoveCharactersContainer(GameManager.Instance._gameData.SelectedCharaID);

		this.HideContainer();

		//Start Menu
		this.ManipulateButtons(true, false, true);
        this.IsBigRouletteShowing = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(this.IsLerping){
			var diff = Time.timeSinceLevelLoad - this.StartTime;
			if (diff > this.EndTime) {
				this.ModelContainer.transform.localPosition = this.EndLocalPos;
				this.IsLerping = false;
			}
			var rate = diff / this.EndTime;
			if(this.IsRouletteSpining)
				rate = diff * 10;
			this.ModelContainer.transform.localPosition = Vector3.Lerp (this.StartLocalPos, this.EndLocalPos, rate);
		}

		if(this.IsAppearing){
			if(this.IsEnlarging){
				this.EnlargingScaleUnit += Time.deltaTime * 500;
				if(this.EnlargingScaleUnit > this.MaxScaleUnit)
					this.IsEnlarging = false;
			}else{
				this.EnlargingScaleUnit -= Time.deltaTime * 1000;
				if(this.EnlargingScaleUnit < this.OriginScaleUnit){
					this.IsAppearing = false;
					this.EnlargingScaleUnit = this.OriginScaleUnit;
					this.RotateSpeedCoef = 1;
				}
			}
			int currentSelectedID = GameManager.Instance._gameData.SelectedCharaID;
			this.Models[currentSelectedID].transform.localScale = Vector3.one * this.EnlargingScaleUnit;
		}

		if(this.IsRouletteSpining){
			this.BigRouletteButton.transform.Rotate(new Vector3(0, 0, 700) * Time.deltaTime);
		}


		if(this.IsButtonEnlarging){
			this.ButtonTransformTime += Time.deltaTime / 10f;
			if(this.ButtonTransformTime > 1.1f)
				this.IsButtonEnlarging = false;
		}else{
			this.ButtonTransformTime -= Time.deltaTime / 10f;
			if(this.ButtonTransformTime < 0.9f)
				this.IsButtonEnlarging = true;
		}
		for(int i=0; i<4; i++){
			this.FourButton[i].transform.localScale = new Vector3(ButtonTransformTime, ButtonTransformTime, 1);
		}
		
		
		this.Models[this.RotatingCharaID].transform.Rotate(new Vector3(0, 90, 0) * Time.deltaTime * this.RotateSpeedCoef);
		
	}

	private void MoveCharactersContainer(int ID){
		this.ModelContainer.SetActive(true);

		int currentSelectedID = GameManager.Instance._gameData.SelectedCharaID;
		this.Models[currentSelectedID].transform.localRotation = Quaternion.Euler(new Vector3(60, 0, 0));
		this.Models[currentSelectedID].transform.localScale = new Vector3(50, 50, 50);

		this.StartTime = Time.timeSinceLevelLoad;
		this.StartLocalPos = this.ModelContainer.transform.localPosition;
		this.EndLocalPos = new Vector3(this.CharaOffset * -ID, 0, 15);
		this.IsLerping = true;

		this.Models[this.RotatingCharaID].transform.localScale = Vector3.one * this.OriginScaleUnit;
		this.Models[this.RotatingCharaID].transform.localRotation = Quaternion.Euler(new Vector3(60, 0, 0));
		this.RotatingCharaID = ID;

		if(!this.IsProcessingRoulette){
			this.ManipulateButtons(true, false, false);
			GameManager.Instance._gameData.SelectedCharaID = ID;
			GameManager.Instance._gameData.SaveChara();
		}else{
			EffectManager.Instance.ApplyEffect("Roulette", Vector3.zero);
		}
	}
	public void ChooseCharacter(int ID){
		if(this.IsProcessingRoulette) return;

		int currentSelectedID = GameManager.Instance._gameData.SelectedCharaID;
		this.MoveCharactersContainer(ID);
		this.CharaNameText.text = LanguageManager.Instance.GetNameText(ID);
		this.Models[currentSelectedID].transform.localScale = new Vector3(70, 70, 70);
	}

	public void HideContainer(){
		this.CharaNameText.text = "";
		this.ModelContainer.SetActive(false);

		this.ManipulateButtons(false, false);
	}

	public void ActivateRoulette(){
		if(this.IsProcessingRoulette){
			if(!this.IsRouletteSpining){
				StartCoroutine("StartRoulette");
			}else{
				this.ManipulateButtons(false, false);
				bool CanStop = GameManager.Instance.ModifyCoinAmount(-500, true, true);
				if(CanStop){
					StartCoroutine("StopRoulette");
				}else{
					//通常起こりえない分岐だが、もし起きてしまったらシーンを再ロードする
					this.CharaNameText.text = LanguageManager.Instance.GetSystemText("Not enough");
					UnityEngine.SceneManagement.SceneManager.LoadScene("Runner");
				}
			}
		}else{

			if(GameManager.Instance._gameData.RouletteCount == 0){
				this.CharaNameText.text = "<color=yellow>" + LanguageManager.Instance.GetSystemText("First time") + "</color>";
				this.IsProcessingRoulette = true;
				this.ActivateRoulette();
				GameManager.Instance.ModifyCoinAmount(500, true, false);


			}else{
				if(GameManager.Instance._gameData.CurrentKeepingCoin >= 500){
					this.CharaNameText.text = LanguageManager.Instance.GetSystemText("Cost 500 stars");
					this.IsProcessingRoulette = true;
					this.ActivateRoulette();

				}else{
					this.CharaNameText.text = LanguageManager.Instance.GetSystemText("Not enough");
				}
			}

		}
	}
	private IEnumerator StartRoulette(){
		this.IsRouletteSpining = true;
		while(this.IsRouletteSpining){
			this.StopID++;
			if(this.StopID > GameManager.TotalCharacterNumber - 1)
				this.StopID = 0;
				
			this.MoveCharactersContainer(this.StopID);
			yield return new WaitForSeconds(0.1f);
		}
	}

	private IEnumerator StopRoulette(){
		this.IsDuringStopPerformance = true;

		GameManager.Instance._gameData.RouletteCount++;
		GameManager.Instance._gameData.Save();
		this.CharaNameText.text = "";

		this.IsRouletteSpining = false;
		bool IsLocked = false;
		for(int i=0; i<5; i++){
			this.StopID++;
			if(this.StopID > GameManager.TotalCharacterNumber - 1)
				this.StopID = 0;

			this.MoveCharactersContainer(this.StopID);
			yield return new WaitForSeconds(0.1f * (i+1));
		}

		int offsetCount = 0;
		while(!IsLocked){
			this.StopID++;
			if(this.StopID > GameManager.TotalCharacterNumber - 1)
				this.StopID = 0;

			this.MoveCharactersContainer(this.StopID);

			//未解禁のキャラクターであれば、そこでルーレットを止める
			if(GameManager.Instance._characterLockManager.UnlockedNumber[this.StopID] == 0)
				IsLocked = true;

			//ルーレットが止まらずに続行される回数は5回まで
			offsetCount++;
			if(offsetCount > 4)
				IsLocked = true;
			yield return new WaitForSeconds(0.7f);
		}

		this.UnlockCharacter(this.StopID);

		yield return new WaitForSeconds(2);
		this.IsProcessingRoulette = false;
		this.ManipulateButtons(true, false);

		this.IsDuringStopPerformance = false;
	}

	private void UnlockCharacter(int ID){
		GameManager.Instance._characterLockManager.UnlockedNumber[ID]++;
		GameManager.Instance._characterLockManager.Save();
		
		GameManager.Instance._gameData.SelectedCharaID = ID;
		GameManager.Instance._gameData.SaveChara();
		this.RotatingCharaID = ID;
		this.MoveCharactersContainer(this.RotatingCharaID);

		if(GameManager.Instance._characterLockManager.UnlockedNumber[ID] == 1){
			Destroy(this.Models[ID]);
			int prefabNumber = GameManager.Instance.CharacterInfoArray[ID].PlayerID;
			this.Models[ID] = Instantiate(Resources.Load("PlayerModels/" + prefabNumber)) as GameObject;
			this.Models[ID].transform.parent = this.ModelContainer.transform;
			this.Models[ID].transform.localPosition = new Vector3(this.CharaOffset * ID, 0, 0);
			this.Models[ID].transform.localScale = Vector3.one * this.OriginScaleUnit;
			this.Models[ID].transform.localRotation = Quaternion.Euler(new Vector3(60, 0, 0));
		
			
			string charaName = LanguageManager.Instance.GetNameText(ID);
			this.CharaNameText.text = LanguageManager.Instance.GetSystemText("Got")
				+ "\n" + charaName;
			
			this.EnlargingScaleUnit = 0;
			this.IsAppearing = true;
			this.IsEnlarging = true;
			this.RotateSpeedCoef = 8;
			EffectManager.Instance.ApplyEffect("Unlocked", Vector3.zero);

			//はじめてキャラを入手した場合、キャラ選択ボタンを表示する
			if(GameManager.Instance._gameData.RouletteCount >= 1){
				if(!GameManager.Instance.MenuButtons[1].activeSelf){
					GameManager.Instance.MenuButtons[1].SetActive(true);
					GameManager.Instance.MenuButtons[2].SetActive(true);
				}
			}

		}else{
			this.CharaNameText.text = LanguageManager.Instance.GetSystemText("Already have")
				+ "\n" + LanguageManager.Instance.GetNameText(ID);
			EffectManager.Instance.ApplyEffect("Damage", Vector3.zero);
		}
	}


	/* Start Menu */


	public void CancelRoulette(){
		if(this.IsDuringStopPerformance) return;//ストップ演出中はもうキャンセルできない

		EffectManager.Instance.ApplyEffect("Button", Vector3.zero);
		this.IsBigRouletteShowing = false;
		this.ManipulateButtons(true, false, true);

		this.IsRouletteSpining = false;
		this.IsProcessingRoulette = false;
		this.CharaNameText.text = "";
		this.ModelContainer.SetActive(false);
	}
    public void TapStartButton(){
		EffectManager.Instance.ApplyEffect("Button", Vector3.zero);
        GameManager.Instance.StartStage();
		this.TutorialButton.SetActive(false);
		this.TappedEffect(0);
    }
	public void TapTutorialButton(){
		EffectManager.Instance.ApplyEffect("Button", Vector3.zero);
		GameManager.Instance.IsTutorial = true;
		GameManager.Instance._tutorialManager.enabled = true;
		GameManager.Instance._tutorialManager.PrepareTutorial();
        GameManager.Instance.StartStage();

		this.TutorialButton.SetActive(false);
		this.TappedEffect(0);
    }
	public void TapSelectButton(bool isRight){
		EffectManager.Instance.ApplyEffect("Button", Vector3.zero);
        GameManager.Instance.ChooseCharacter(isRight);
		if(isRight){
			this.TappedEffect(2);
		}else{
			this.TappedEffect(1);
		}
    }
	public void TapRouletteButton(){
		if(!this.FourButton[3].activeSelf && !this.BigRouletteButton.activeSelf) return;
		if(this.IsDuringStopPerformance) return;

		EffectManager.Instance.ApplyEffect("Button", Vector3.zero);
		this.IsBigRouletteShowing = !this.IsBigRouletteShowing;
		this.ManipulateButtons(!this.IsBigRouletteShowing, this.IsBigRouletteShowing, false);
		this.ActivateRoulette();

		this.TutorialButton.SetActive(false);

		this.TappedEffect(3);
		
    }
	private void TappedEffect(int number){
		//今の所何もすることはない
	}
    public void ManipulateButtons(bool fourButtons = false, bool bigRoulette = false, bool titleLogo = false){
        this.FourButtonsContainer.SetActive(fourButtons);
		this.TutorialButton.SetActive(fourButtons);

        this.BigRouletteButton.SetActive(bigRoulette);
		// this.BackButton.SetActive(bigRoulette);

		this.TitleLogo.SetActive(titleLogo);
    }
}
