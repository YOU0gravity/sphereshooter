using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// タッチなどの入力をコントロールする
/// </summary>
public class OperationManager : MonoBehaviour {
    private Vector3 TouchBeginPos;
    private bool IsHolding;
	[SerializeField] private ParticleSystem TapEffect;
    [SerializeField] private Camera EffectCamera;
    [SerializeField] private TutorialManager TutorialManagerInstance;


    private int LastKey;
    private int CommandCount;
    //上上下下左右左右BAでデバッグモード！
    private int[] SecretCommand = new int[]{0, 0, 3, 3, 1, 2, 1, 2, 4, 1};
    [SerializeField] private Text PauseText;

    void Update(){
        if(!GameManager.Instance.IsPausing && GameManager.IsStageStarted){
            if (Input.GetMouseButtonDown(0) ) {
                EventSystem current = EventSystem.current;
                if(current != null){
                    if(current.currentSelectedGameObject != null)
                        return;
                }
                this.TouchBeginPos = Input.mousePosition;
                this.IsHolding = true;
            }

            if(Input.GetMouseButton(0)){
                this.TapEffect.transform.position = this.EffectCamera.ScreenToWorldPoint(Input.mousePosition + this.EffectCamera.transform.forward * 10);
                this.TapEffect.Emit(1);
            }

            if(Input.GetMouseButtonUp(0)){
                Vector3 endPos = Input.mousePosition;
                Vector3 beginPos = TouchBeginPos;
                this.ButtonUp(beginPos, endPos);
            }

            if(Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow)){//↑
                this.IsHolding = true;
                this.ButtonUp(Vector3.zero, Vector3.up * 101);
            }
            if(Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow)){//←
                this.IsHolding = true;
                this.ButtonUp(Vector3.zero, Vector3.left * 101);
            }
            if(Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow)){//↓
                this.IsHolding = true;
                this.ButtonUp(Vector3.zero, Vector3.down * 101);
            }
            if(Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow)){//→
                this.IsHolding = true;
                this.ButtonUp(Vector3.zero, Vector3.right * 101);
            }

            if(Input.GetKey(KeyCode.Space)){
                this.IsHolding = true;
                this.ButtonUp(Vector3.zero, Vector3.zero);
            }

        }else{
            this.LastKey = -1;
            if(Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
                this.LastKey = 0;
            if(Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
                this.LastKey = 1;
            if(Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
                this.LastKey = 3;
            if(Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
                this.LastKey = 2;
            if(Input.GetKeyDown(KeyCode.B))
                this.LastKey = 4;

            if(CommandCount < 10){
                if(this.LastKey == -1) return;

                if(this.SecretCommand[CommandCount] == this.LastKey){
                    CommandCount++;
                }else{
                    CommandCount = 0;
                }
            }else{
                if(CommandCount > 10) return;

                this.PauseText.fontSize = 40;
                this.PauseText.text = "DEBUG MODE! During playing the stage, press number key!";
                EffectManager.Instance.ApplyEffect("Unlocked");
                CommandCount++;
            }
        }



    }

    private void ButtonUp(Vector3 beginPos, Vector3 endPos){
        if(this.IsHolding){
            if(!GameManager.Instance.IsTutorial){
                GameManager.Instance.CallAppropriateTouchOperation(beginPos, endPos);
            }else{
                this.TutorialManagerInstance.CallAppropriateTouchOperation(beginPos, endPos);                   
            }
            this.IsHolding = false;
        }
    }


}