﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// カメラを揺らします
/// </summary>
public class CameraShaker : MonoBehaviour {
	[SerializeField] private GameObject CameraObject;
	private float ShakeDecay = 0.004f;
	private Quaternion OriginRot;
	private float ShakeIntensity;

	void Start(){
		this.OriginRot = this.CameraObject.transform.localRotation;
	}
	// Update is called once per frame
	void Update () {

		
		if(this.ShakeIntensity > 0){

	
			Vector3 shakedVector = Random.insideUnitSphere * ShakeIntensity;
			shakedVector.z = 0;
			CameraObject.transform.localPosition = shakedVector;
			CameraObject.transform.localRotation = new Quaternion(OriginRot.x + Random.Range(-ShakeIntensity, ShakeIntensity),
									OriginRot.y + Random.Range(-ShakeIntensity, ShakeIntensity),
									OriginRot.y + Random.Range(-ShakeIntensity, ShakeIntensity),
									OriginRot.w + Random.Range(-ShakeIntensity, ShakeIntensity));
			
			this.ShakeIntensity -= this.ShakeDecay;

		}else if(this.ShakeIntensity < 0){
			this.ShakeIntensity = 0;
			this.CameraObject.transform.localPosition = Vector3.zero;
			this.CameraObject.transform.localRotation = this.OriginRot;
		}
	}

	public void Shake(float intensity){
		this.ShakeIntensity = intensity;
	}
}
