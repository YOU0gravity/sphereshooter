﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// プレイヤーの位置や移動などをコントロールするクラス
/// </summary>
public class PlayerController : MonoBehaviour {
	
	[SerializeField] private Animator PlayerAnimator;
	private GameObject BulletPrefab;

	public bool IsStealthState = false;


	public int XLaneNum = 1;
	public int YLaneNum = 1;
	private readonly int LaneLimitNumber = 2;

	//プレイヤーの能力値
	public float MoveSpeed = 10f;
	private float ShootOffset = 1;

	//Enchant
	private Coroutine QuickMoveCoroutine;
	private Coroutine InvincibleDashCoroutine;

	private Vector3 endLocalPosition;
	private Vector3 startLocalPosition;
	private float startTime;


	private float FloatingPositionY;
	private bool IsFloatingToUp;

	private bool IsDuringDamageMotion;
	private float DamageRotation;
	private Quaternion CurrentLocalRotation;

	private float AutoShootTime;


	private bool IsEnlargingScale;
	private float QuickMoveScale;


	private BoxCollider PlayerCollider;
	private bool IsDuringQuickMove;
	private float SpeedRetention;


	void Start () {
		this.PlayerCollider = GetComponentInChildren<BoxCollider>();
		this.ShootOffset = GameManager.Instance.SelectedChara.ShootOffset;
		this.BulletPrefab = Resources.Load("Weapons/" + GameManager.Instance.SelectedChara.PlayerID.ToString()) as GameObject;

		this.IsFloatingToUp = true;
		Vector3 a = transform.position;
		a.y = 0;
		a.x = 0;
		transform.position = a;
	}

	public void SetParameter(CharacterInfo ci){
		this.ShootOffset = ci.ShootOffset;
		this.BulletPrefab = Resources.Load("Weapons/" + ci.PlayerID.ToString()) as GameObject;
	}
	void Update(){
		if(GameManager.IsStageStarted){
			if(this.PlayerAnimator)
				this.PlayerAnimator.SetFloat("Speed", 5f);

			if(this.IsDuringDamageMotion){
				this.DamageRotation += Time.deltaTime * 360;
				this.CurrentLocalRotation = Quaternion.Euler(new Vector3(0, this.DamageRotation, 0));
				GameManager.Player3DModel.transform.localRotation = this.CurrentLocalRotation;
			}
		
			if(GameManager._smoothCameraMover.enabled){
				var diff = Time.timeSinceLevelLoad - startTime;
				var rate = diff / 1;
				transform.localPosition = Vector3.Lerp (startLocalPosition, endLocalPosition, rate);

			}else{
				this.AutoShootTime += Time.deltaTime;
				if(this.AutoShootTime > this.ShootOffset){
					this.AutoShootTime = 0;

					Vector3 insPos = new Vector3((this.XLaneNum - 1) * 5, (this.YLaneNum - 1) * 5, GameManager.Player3DModel.transform.position.z + 1);
					GameObject bullet = Instantiate(BulletPrefab, insPos, transform.rotation) as GameObject;
					bullet.GetComponent<BulletController>().SetParameter(GameManager.Instance.SelectedChara);
					bullet.transform.SetParent(GameManager.PlayerFollowingContainer.transform);

					GameManager.Instance.CurrentShootCount++;
					EffectManager.Instance.ApplyEffect("Shoot");
				}

			}

		}


		if(GameManager.Player3DModel != null){
			// switch(GameManager.Instance.GameMode){
			// 	case 0:
					//浮き沈みするアニメーション
					if(this.IsFloatingToUp){
						this.FloatingPositionY += Time.deltaTime / 3f;
						if(this.FloatingPositionY > -0.75f){
							this.IsFloatingToUp = false;
						}
					}else{
						this.FloatingPositionY -= Time.deltaTime / 3f;
						if(this.FloatingPositionY < -1.25f){
							this.IsFloatingToUp = true;
						}
					}
					GameManager.Player3DModel.transform.localPosition = new Vector3(0,this.FloatingPositionY,0);
			// 		break;
			// }

			if(this.IsDuringQuickMove){
				var rate = (Time.timeSinceLevelLoad - startTime) * 8;
				transform.localPosition = Vector3.Lerp (startLocalPosition, endLocalPosition, rate);

				// if(GameManager.Instance.GameMode == 0){
					if(this.IsEnlargingScale){
						if(this.QuickMoveScale < 1.3f)
							this.QuickMoveScale += Time.deltaTime * 3;
					}else{
						if(this.QuickMoveScale > 0.3f)
							this.QuickMoveScale -= Time.deltaTime * 3;
					}
					transform.localScale = new Vector3(QuickMoveScale, 1, 1);
				// }

			}
		}
	}

	public void Flick(Vector3 moveVector){
		if(GameManager._smoothCameraMover.enabled) return;

		bool isHorizontalMove = true;
		if(System.Math.Abs(moveVector.y) > System.Math.Abs(moveVector.x))
			isHorizontalMove = false;

		if(isHorizontalMove){//横方向へのフリック

			switch(GameManager.DimensionNumber){
				case 0: case 2: case 4:
					if(moveVector.x > 0){//Right
						if(this.XLaneNum + 1 <= this.LaneLimitNumber)
							this.XLaneNum++;
						
					}else{//Left
						if(this.XLaneNum - 1 >= 0)
							this.XLaneNum--;	
					}
					break;
				
			}

		}else{//縦方向へのフリック
			switch(GameManager.DimensionNumber){
				case 0: case 1: case 3:
					if(moveVector.y > 0){//Up
						if(this.YLaneNum + 1 <= this.LaneLimitNumber)
							this.YLaneNum++;

					}else{//Down
						if(this.YLaneNum - 1 >= 0)
							this.YLaneNum--;
					}
					break;
			
			}
		}


		Vector3 localPlayerPos = transform.localPosition;
		this.startLocalPosition = localPlayerPos;
		if(GameManager.DimensionNumber == 3){
			localPlayerPos.x = 0;
		}else{
			localPlayerPos.x = StageManager.AreaLength * (this.XLaneNum - 1);
		}
		if(GameManager.DimensionNumber == 2){
			localPlayerPos.y = 0;
		}else{
			localPlayerPos.y = StageManager.AreaLength * (this.YLaneNum - 1);
		}

		localPlayerPos.z = 0;
		this.endLocalPosition = localPlayerPos;
		startTime = Time.timeSinceLevelLoad;

		
		if(this.QuickMoveCoroutine != null){
			StopCoroutine(this.QuickMoveCoroutine);
		}
		this.QuickMoveCoroutine = StartCoroutine("SetQuickMoveTime");
	}



	private IEnumerator SetQuickMoveTime(){
		//速度が一定以上の場合のみ、僅かな時間だが無敵時間が発生するようにしている
		if(this.MoveSpeed > 40)
			this.IsStealthState = true;

		this.IsDuringQuickMove = true;

		this.IsEnlargingScale = false;
		this.QuickMoveScale = 1;

		yield return new WaitForSeconds(0.05f);

		this.IsStealthState = false;

		yield return new WaitForSeconds(0.10f);
		this.IsEnlargingScale = true;

		yield return new WaitForSeconds(0.25f);
		this.IsDuringQuickMove = false;

		transform.localScale = new Vector3(1, 1, 1);
	}

	public void BecomeInvincible(bool haveDamaged, float time){
		if(this.InvincibleDashCoroutine != null){
			StopCoroutine(this.InvincibleDashCoroutine);
		}
		this.InvincibleDashCoroutine = StartCoroutine(InvincibleDash(haveDamaged, time));
	}

	private IEnumerator InvincibleDash(bool haveDamaged, float time){
		GameManager.Instance.IsInvincible = true;

		if(haveDamaged)
			this.IsDuringDamageMotion = true;

		yield return new WaitForSeconds(time);

		GameManager.Instance.IsInvincible = false;
		this.IsDuringDamageMotion = false;
	}


	public void ChangeDimensions(int dimensionNumber){
		this.SpeedRetention = this.MoveSpeed;
		this.MoveSpeed = 0;

		startTime = Time.timeSinceLevelLoad;		
		Vector3 startPos = new Vector3(StageManager.AreaLength * (this.XLaneNum - 1),  StageManager.AreaLength * (this.YLaneNum - 1), 0);
		Vector3 endPos = new Vector3(StageManager.AreaLength * (this.XLaneNum - 1), StageManager.AreaLength * (this.YLaneNum - 1), 0);
		switch(dimensionNumber){
			case 2:
				endPos.y = 0;
				break;
			case 3:
				endPos.x = 0;
				break;
		}
		startLocalPosition = startPos;
		endLocalPosition = endPos;
	}

	public void OnDimensionChanged(int dimensionNumber){
		this.MoveSpeed = this.SpeedRetention;
		this.BecomeInvincible(false, 0.3f);

		switch(dimensionNumber){
			case 0:
				this.PlayerCollider.size = new Vector3(0.5f, 0.5f, 0.5f);
				break;
			case 1: case 3:
				this.PlayerCollider.size = new Vector3(40, 0.5f, 0.5f);
				break;
			case 2: case 4:
				this.PlayerCollider.size = new Vector3(0.5f, 40, 0.5f);
				break;
		}
	}
}

[System.Serializable]
public class CharacterInfo {
	public int PlayerID { get; set;}
	public int GameBackground { get; set; }
	public float ShootOffset { get; set; }
	public int[] Enchant { get; set; }
	public float WeaponSpeed { get; set; }
	public float WeaponDistance { get; set; }
	public float WeaponAttack { get; set; }


	//Enchantに格納される数字は３桁の整数
	//最初の桁(100)は、処理する場所を表す(1:BulletController, 2:PlayerController, 3:GameManager)
	//次の桁(10)は、付与するエンチャントのID
	//最後の桁(1)は、付与するエンチャントのレベル
	//ex. 323であれば、GameManager内で処理する"ToughBody"というエンチャントのレベルを3にする
	//ex. 111であれば、BulletController内で処理する"Penetration"というエンチャントのレベルを1にする
	private Dictionary<int, string> EnchantDic = new Dictionary<int, string>(){
		{0, "None,---"},
		{11, "PENETRATION"},
		{21, "COIN SAVER"},
		{31, "TRESURE HUNTER"},{32, "PSYCHO KILLER"},
	};
	private string EnchantDesc(int id, int lv){
		string str = "---";
		switch(id){
			case 11: str = "弾が敵" + lv + "体まで貫通する"; break;
			case 21: str = "コイン消費-" + lv + "枚"; break;
			case 31: str = "コイン取得" + lv * 10 + "%UP"; break;
			case 41: str = "敵" + (20 - 2 * lv) + "体撃破毎にHP回復"; break;
		}
		return str;
	}
	public CharacterInfo(){
		this.PlayerID = 0;
		this.GameBackground = 0;
		this.ShootOffset = 0;
		this.Enchant = new int[3];
		for(int i=0; i<3; i++)
			this.Enchant[i] = 0;

		this.WeaponSpeed = 0;
		this.WeaponDistance = 0;
		this.WeaponAttack = 0;
	}

	public void RegisterNewCharacter(int playerID, int gameBackground = 0, float shootOffset = 0, float weaponSpeed = 0, float weaponDistance = 0, float weaponAttack = 0, int enchant0 = 0, int enchant1 = 0, int enchant2 = 0){
		this.PlayerID = playerID;
		this.GameBackground = gameBackground;
		this.ShootOffset = shootOffset;
		this.WeaponSpeed = weaponSpeed;
		this.WeaponDistance = weaponDistance;
		this.WeaponAttack = weaponAttack;
		this.Enchant[0] = enchant0;
		this.Enchant[1] = enchant1;
		this.Enchant[2] = enchant2;
	}

	public string ExportAsString(){
		var sb = new System.Text.StringBuilder();
		sb.Append("<WeaponSpec>");
		sb.AppendLine();
		sb.Append("Speed: ");
		sb.Append(this.WeaponSpeed);
		sb.AppendLine();
		sb.Append("Distance: ");
		sb.Append(this.WeaponDistance);
		sb.AppendLine();
		sb.Append("Attack: ");
		sb.Append(this.WeaponAttack);
		sb.AppendLine();
		sb.Append("ShootOffset: ");
		sb.Append(this.ShootOffset.ToString("F3"));
		sb.AppendLine();
		sb.AppendLine();

		sb.Append("<Ability>");
		sb.AppendLine();
		for(int i=0; i<3; i++){
			sb.Append("@");
			int id = this.Enchant[i] / 10;
			int level = this.Enchant[i] % 10;
			sb.Append(this.EnchantDic[id]);
			sb.Append(" Lv.");
			sb.Append(level);
			sb.AppendLine();
			sb.Append("(");
			sb.Append(this.EnchantDesc(id, level));
			sb.Append(")");
			sb.AppendLine();
			sb.AppendLine();
		}
		return sb.ToString();
	}
}