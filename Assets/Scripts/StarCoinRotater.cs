﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// スターコインを回転させたり、Dimension変化時に移動させたりします
/// </summary>
public class StarCoinRotater : MonoBehaviour {
	[SerializeField] private GameObject CoinModel;
	private float RotateSpeed = 180;
	private Vector3 OriginPos;
	private float StartTime;
	private Vector3 EndPos;
	private Vector3 StartPos;

	void Start(){
		this.OriginPos = transform.position;
	}

	void Update () {
		this.CoinModel.transform.Rotate(new Vector3(0, this.RotateSpeed, 0) * Time.deltaTime);
		if(GameManager._smoothCameraMover.enabled){
			var diff = Time.timeSinceLevelLoad - this.StartTime;
			if (diff > 1) {
				transform.position = this.EndPos;
			}
			var rate = diff / 1;
			transform.position = Vector3.Lerp (this.StartPos, this.EndPos, rate);
		}
	}

	public void ChangeDimensions(int dimensionNumber){
		this.StartTime = Time.timeSinceLevelLoad;
		this.StartPos = transform.position;
		switch(dimensionNumber){
			case 0:
				this.EndPos = this.OriginPos;
				break;
			case 1: case 3:
				this.EndPos = new Vector3(0, this.OriginPos.y, this.OriginPos.z);
				break;
			case 2: case 4:
				this.EndPos = new Vector3(this.OriginPos.x, 0, this.OriginPos.z);
				break;
		}
	}

	public void OnTriggerEnter(Collider other){
		if(other.gameObject.tag == "Player"){
			GameManager.Instance.GetCoin(transform.position, 1, true);
			Destroy(gameObject);
		}
	}
}
