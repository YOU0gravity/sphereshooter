using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// チュートリアルを管理するクラス
/// </summary>
public class TutorialManager : MonoBehaviour {
    [SerializeField] private GameObject TouchIcon;
	[SerializeField] private TouchIconAnimationController TIACScript;
    [System.NonSerializedAttribute] public int TutorialStep;

	private bool IsTouchIconFlicking;
	private bool IsTouchIconTaping;

	private List<GameObject> TutorialObjects;

	[SerializeField] private Text GameText;
	[SerializeField] private Image GameTextBackground;

	private bool IsWaitingForTap;
	private bool IsWaitingForMission;
	private bool IsMissionCompleted;
	private bool CanFlickDuringTutorial;
    private bool CanTapDuringTutorial;
	private float MissionCheckTime;


	private bool IsDuringTextAnimation;
	private GameObject IllustrativeCube;


	private static readonly string[,] TutorialLanguageValue = new string[,]{
		//English
		{
			"Welcome to the world of\nSphere Shooter!",
			"All you have to do is destroy '●'s.", 
			"Hit '●'s by your shot.", 
			"Flick to move\nand destroy them!",
			"You can get score points\nand coins by destroying them.",

			"The purpose of this game is\nto get highscore.",
			"You can't destroy\nnot '●' shaped objects.",
			"Flick to avoid them!",
			"There is no space\nto avoid them.",
			"But it's ok.\nYou have a special power.",

			"Tap the screen!",
			"You can change\nthe point of view.",
			"It makes them\nchange their shape.",
			"If you find non-'●' objects,\ntap the screen.",
			"Let's do it one more time.",

			"Tap to change\nthe point of view.",
			"You completed\nthe basic of Sphere Shooter.",
			"Finally,\nlook at them.",
			"These are coins.\nYou can collect them.",
			"Also, you can get them\nby destroying '●'s.",

			"Collect some coins.",
			"Collect a lot of coins,\nand unlock new characters.",
			"Well, that's it.\nThis is the end.",
			"Have a good time!"
		},

		//日本語
		{
			"ようこそ\nスフィアシューターの世界へ！",
			"このゲームは　'●'を\nこわすゲームです",
			"'●'に　こうげきすると\nこわせます",
			"←や→へフリックして\n'●'を　こわそう",
			"'●'を　こわすと、\nスコアとコインをゲット",

			"たくさんスコアを\nあつめるのが目的です",
			"'●'ではないものは、\nこわせません",
			"←や→へフリックして、\nよけよう",
			"よけられる　ばしょが\nありませんね",
			"でも大丈夫\nしてんを　かえてみましょう",

			"がめんをタップ！",
			"ウエから　みていたモノを\nヨコから　みることで",
			"モノの　ばしょや\nカタチが　かわります",
			"こわせないモノを　みつけたら\nタップでカタチを　かえましょう",
			"もういちど\nやってみましょう",

			"タップで　してんへんこう\nフリックで　いどう",
			"これで　せつめいは　おわりです",
			"・・・あ\nさいごに　ひとつ",
			"これはコイン\nふれると　入手できます",
			"'●'を　こわす　ことでも\n入手できます",
			
			"コインを　あつめよう",
			"たくさん　あつめると\nいいことが　あります！",
			"・・・さて\nこれで　せつめいは　おわりです",
			"スフィアシューターを\nたのしんで　いってください！"
		}
	};
	private int TutorialCount;

	
	void Update(){
		if(this.IsWaitingForMission){
			this.MissionCheckTime += Time.deltaTime;
			if(this.MissionCheckTime > 0.2f){
				this.MissionCheckTime = 0;
				if(this.IsMissionCompleted){
					this.TutorialStep++;
					StartCoroutine("ProgressTutorial", 0.2f);
				}
			}
		}
	}

	public void PrepareTutorial(){
		this.TutorialObjects = new List<GameObject>();
		this.TouchIcon.SetActive(false);

		StageData.Instance.AppearEnemiesArray = new int[]{1};
		StageData.Instance.CoinAppearancePercentage = 0;
		StageData.Instance.CoinPlaceInSameLinePercentage = 0;
		StageData.Instance.EnemyAppearancePercentage = 0;
		StageData.Instance.StageLength = 3;
		StageData.Instance.SpeedUpCoef = 0.2f;
		StageData.Instance.CreateRoadsInfoRandomly();
	}
	public void StartTutorial(){
		StartCoroutine("ProgressTutorial", 0.2f);
	}

	private IEnumerator ProgressTutorial(float time = 0.2f){
		float waitingTime = time;
		this.IsWaitingForTap = false;
		this.IsWaitingForMission = false;
		this.TouchIcon.SetActive(false);
		switch(this.TutorialStep){
			case 0: SetTextAndWaitTap(ViewNextMessage(), viewTouchIcon: true); break;
			case 1: SetTextAndWaitTap(ViewNextMessage(), viewTouchIcon: true); break;
			case 2: SetTextAndWaitTap(ViewNextMessage(), viewTouchIcon: true); break;
			case 3:
				SetTextAndWaitTap(ViewNextMessage(), true, timeScale: 1, isYellowColor: true);
				float posZ = GameManager.PlayerWorldPosContainer.transform.position.z;
				for(int z=0; z<6; z++){
					for(int x=0; x<3; x++){
						if(x == z % 3){
							GameObject a = Instantiate(Resources.Load("Stages/Space/Sphere"), new Vector3(x * 5 - 5, 0, posZ + 45 + z * 5), Quaternion.identity) as GameObject;
							this.TutorialObjects.Add(a);
						}
					}
				}
				GameObject goal = Instantiate(Resources.Load("Stages/GoalPoint"), new Vector3(0, 0, posZ + 55), Quaternion.identity) as GameObject;
				this.TutorialObjects.Add(goal);

				this.CanFlickDuringTutorial = true;
				break;
			
			case 4: SetTextAndWaitTap(ViewNextMessage()); break;

			case 5: SetTextAndWaitTap(ViewNextMessage()); break;
			case 6: SetTextAndWaitTap(ViewNextMessage()); break;
			case 7: 
				SetTextAndWaitTap(ViewNextMessage(), true, timeScale: 1, isYellowColor: true);
				posZ = GameManager.PlayerWorldPosContainer.transform.position.z;
				for(int i=0; i<3; i++){
					int posX = 0;
					int posZOffset = 0;
					switch(i){ case 1: posX = -5; posZOffset = 10; break; case 2: posX = 5; posZOffset = 20; break;}
					GameObject a = Instantiate(Resources.Load("Stages/Space/CylinderH"), new Vector3(posX, 0, posZ + 40 + posZOffset), Quaternion.identity) as GameObject;
					this.TutorialObjects.Add(a);
				}
				goal = Instantiate(Resources.Load("Stages/GoalPoint"), new Vector3(0, 0, posZ + 70), Quaternion.identity) as GameObject;
				this.TutorialObjects.Add(goal);
				break;

			case 8: waitingTime = 0; this.TutorialStep++; StartCoroutine("ProgressTutorial", 0.2f); break;
			case 9: 
				SetTextAndWaitTap("", true, timeScale: 1);
				posZ = GameManager.PlayerWorldPosContainer.transform.position.z;
				for(int y=0; y<3; y++){
					for(int x=0; x<3; x++){
						if(x == y){
							GameObject a = Instantiate(Resources.Load("Stages/Space/CylinderH"), new Vector3(x * 5 - 5, y * 5 - 5, posZ + 40), Quaternion.identity) as GameObject;
							this.TutorialObjects.Add(a);
						}
					}
				}
				goal = Instantiate(Resources.Load("Stages/GoalPoint"), new Vector3(0, 0, posZ + 25), Quaternion.identity) as GameObject;
				this.TutorialObjects.Add(goal);
				goal = Instantiate(Resources.Load("Stages/GoalPoint"), new Vector3(0, 0, posZ + 45), Quaternion.identity) as GameObject;
				this.TutorialObjects.Add(goal);
				break;
				
			case 10: SetTextAndWaitTap(ViewNextMessage()); break;
			case 11: SetTextAndWaitTap(ViewNextMessage()); break;
			case 12:
				SetTextAndWaitTap(ViewNextMessage(), true, timeScale: 0.2f, isYellowColor: true);
				yield return new WaitForSecondsRealtime(0.1f);
				this.CanTapDuringTutorial = true;
				waitingTime = 0;
				break;
			case 13:
				this.CanTapDuringTutorial = false;
				SetTextAndWaitTap("", true, timeScale: 1);
				this.GameTextBackground.enabled = false;
				break;
			case 14:
				SetTextAndWaitTap(ViewNextMessage());
				posZ = GameManager.PlayerWorldPosContainer.transform.position.z;
				GameObject cube = Instantiate(Resources.Load("MiniCubeParent"), Vector3.zero, Quaternion.identity) as GameObject;
				cube.transform.parent = GameManager.PlayerFollowingContainer.transform;
				cube.transform.localPosition = new Vector3(0, -23, 13);
				cube.transform.localRotation = Quaternion.Euler(-90, 0, -90);
				this.IllustrativeCube = cube;
				break;
			case 15: SetTextAndWaitTap(ViewNextMessage()); break;
			case 16: SetTextAndWaitTap(ViewNextMessage()); break;
			case 17: SetTextAndWaitTap(ViewNextMessage()); break;
			case 18: 
				SetTextAndWaitTap(ViewNextMessage(), true, timeScale: 1, isYellowColor: true);
				yield return new WaitForSecondsRealtime(0.1f);
				this.CanTapDuringTutorial = true;
				waitingTime = 0;
				
				posZ = GameManager.PlayerWorldPosContainer.transform.position.z;
				for(int y=0; y<3; y++){
					for(int x=0; x<3; x++){
						if(x == y){
							GameObject a = Instantiate(Resources.Load("Stages/Space/CylinderV"), new Vector3(x * 5 - 5, y * 5 - 5, posZ + 40), Quaternion.identity) as GameObject;
							this.TutorialObjects.Add(a);
						}
					}
				}
				goal = Instantiate(Resources.Load("Stages/GoalPoint"), new Vector3(0, 0, posZ + 45), Quaternion.identity) as GameObject;
				this.TutorialObjects.Add(goal);

				Destroy(this.IllustrativeCube);
				break;
			case 19:
				this.CanTapDuringTutorial = false;
				SetTextAndWaitTap(ViewNextMessage());
				break;
			case 20: SetTextAndWaitTap(ViewNextMessage()); break;
			case 21: 
				SetTextAndWaitTap("", true, timeScale: 1);

				posZ = GameManager.PlayerWorldPosContainer.transform.position.z;
				for(int z=0; z<3; z++){
					for(int y=0; y<3; y++){
						for(int x=0; x<3; x++){
							GameObject a = Instantiate(Resources.Load("Money"), new Vector3(x * 5 - 5, y * 5 - 5, posZ + 30 + z * 5), Quaternion.identity) as GameObject;
							this.TutorialObjects.Add(a);
						}
					}
				}
				goal = Instantiate(Resources.Load("Stages/GoalPoint"), new Vector3(0, 0, posZ + 20), Quaternion.identity) as GameObject;
				this.TutorialObjects.Add(goal);
				goal = Instantiate(Resources.Load("Stages/GoalPoint"), new Vector3(0, 0, posZ + 50), Quaternion.identity) as GameObject;
				this.TutorialObjects.Add(goal);
				break;
				
			case 22: SetTextAndWaitTap(ViewNextMessage()); break;
			case 23: SetTextAndWaitTap(ViewNextMessage()); break;
			case 24: 
				SetTextAndWaitTap(ViewNextMessage(), true, timeScale: 1f, isYellowColor: true);
				yield return new WaitForSecondsRealtime(0.1f);
				waitingTime = 0;
				break;
			case 25: SetTextAndWaitTap(ViewNextMessage()); break;
			case 26: SetTextAndWaitTap(ViewNextMessage()); break;
			case 27: SetTextAndWaitTap(ViewNextMessage()); break;
			case 28:
				SetTextAndWaitTap("", false, timeScale: 1);
				this.CanTapDuringTutorial = true;
				this.GameTextBackground.enabled = false;
				this.EndTutorial();
				break;
		}
		yield return new WaitForSecondsRealtime(waitingTime);
	
		
		//TODO: タップでメッセージを送れるような表示を作る
	}

	private string ViewNextMessage()
	{
		return TutorialLanguageValue[LanguageManager.LanguageID, TutorialCount++];
	}

	private void SetTextAndWaitTap(string text, bool waitForMission = false, float timeScale = 0, bool isYellowColor = false, bool viewTouchIcon = false){
		StartCoroutine(AnimateText(text, isYellowColor, viewTouchIcon));
		if(waitForMission){
			this.IsWaitingForMission = true;
			this.IsMissionCompleted = false;
			this.MissionCheckTime = 0f;
			this.GameTextBackground.enabled = false;	

		}else{
			this.IsWaitingForTap = true;
			this.GameTextBackground.enabled = true;
		}
		Time.timeScale = timeScale;
	}
	private IEnumerator AnimateText(string text, bool isYellowColor, bool viewTouchIcon = false){
		this.IsDuringTextAnimation = true;
		if(text.Length > 0){
			for(int i=0; i<text.Length; i++){
				string viewingText = "";
				if(isYellowColor)
					viewingText = "<color=yellow>";
				viewingText += text.Substring(0,i+1);
				if(isYellowColor)
					viewingText += "</color>";
				GameText.text = viewingText;
				
				EffectManager.Instance.ApplyEffect("Key");
				
				yield return new WaitForSecondsRealtime(0.05f);
			}
		}else{
			GameText.text = "";
		}
		this.IsDuringTextAnimation = false;
		if(viewTouchIcon){
			this.TouchIcon.SetActive(true);
			this.TIACScript.StartTapping();
		}
	}
	private void ActivateTouchIcon(bool flicking){
		this.TouchIcon.SetActive(true);
		if(flicking){
			this.IsTouchIconFlicking = true;
			this.TIACScript.StartFlicking();
		}else{
			this.IsTouchIconTaping = true;
			this.TIACScript.StartTapping();
		}
	}

	private void DidFlick(Vector3 beginPos, Vector3 endPos){
		//特に今の所呼び出してもすることがない
	}

	private void DidTap(){
		if(this.IsWaitingForTap){
			if(this.IsDuringTextAnimation){
				//TODO: 文字アニメーションスキップ
			}else{
				this.TutorialStep++;
				StartCoroutine("ProgressTutorial", 0.2f);
			}
		}

		if(this.IsWaitingForMission){
			switch(this.TutorialStep){
				case 12:
					if(this.CanTapDuringTutorial){
						Time.timeScale = 1;
						this.TutorialStep++;
						StartCoroutine("ProgressTutorial", 0.2f);
					}
					break;
			}
		}
	}
	public void DidPassGoal(){
		if(this.IsWaitingForMission){
			switch(this.TutorialStep){
				case 3: case 7: case 9: case 13: case 18: case 21: case 24:
					this.IsMissionCompleted = true;
					break;
			}
		}
	}

	public void CallAppropriateTouchOperation(Vector3 beginPos, Vector3 endPos){
		float moveValueX = endPos.x - beginPos.x;
		float moveValueY = endPos.y - beginPos.y;
		float hypotenuse = Mathf.Sqrt(moveValueX * moveValueX + moveValueY * moveValueY);
		if(hypotenuse > 30){
			if(this.CanFlickDuringTutorial)
				GameManager.Instance.CallAppropriateTouchOperation(beginPos, endPos);
			this.DidFlick(beginPos, endPos);

		}else{
			if(this.CanTapDuringTutorial)
				GameManager.Instance.CallAppropriateTouchOperation(beginPos, endPos);
			this.DidTap();
		}
	}

	private void EndTutorial(){
		foreach(GameObject obj in this.TutorialObjects){
			Destroy(obj);
		}
		GameManager.Instance.IsTutorial = false;
		GameManager.Instance.StartStage();
		Destroy(this);
	}
}