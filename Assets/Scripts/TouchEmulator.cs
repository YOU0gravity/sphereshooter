﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// スマホ以外のプラットフォームで、キーボードで操作できるようにするクラス
/// </summary>
public class TouchEmulator : MonoBehaviour {
	[SerializeField] private GameObject[] Buttons;
	[SerializeField] private GameObject Cursor;
	private int SelectingNumber;

	[SerializeField] private SelectingCharacterController SelectingCharacterControllerInstance;
	void Start () {
		//TODO: スマホなら自壊する
		this.SelectingNumber = -1;
	}
	
	void Update () {
		if(GameManager.Instance.IsPausing) return;
		if(GameManager.Instance.PressedStartButton){
			if(this.Cursor.activeSelf){
				this.Cursor.SetActive(false);
			}
		}
		
		if(GameManager.Instance.IsLogoAnimationEnded){
			if(Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow)){//↑
				this.Select(0);
			}
			if(Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow)){//←
				this.Select(1);
			}
			if(Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow)){//→
				this.Select(2);
			}
			if(Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow)){//↓
				this.Select(3);
			}
			if(Input.GetKeyDown(KeyCode.Escape)){
				if(this.SelectingCharacterControllerInstance.IsProcessingRoulette){
					this.SelectingCharacterControllerInstance.CancelRoulette();
				}
			}
		}
		if(Input.GetKeyDown(KeyCode.Space)){
			this.Enter();
		}
	}
	private void Select(int number){
		if(!GameManager.Instance.PressedStartButton){
			if(!this.SelectingCharacterControllerInstance.IsProcessingRoulette){
				EffectManager.Instance.ApplyEffect("Roulette");
				if(!this.Buttons[number].activeSelf){
					number = 0;
				}

				if(!this.Cursor.activeSelf)
					this.Cursor.SetActive(true);
				this.SelectingNumber = number;
				this.Cursor.transform.position = this.Buttons[number].transform.position;
			}else{
				Debug.Log("ルーレットが回っているので、他のボタンを選択できません");
			}
		}
	}
	private void Enter(){
		if(!GameManager.Instance.PressedStartButton){
			switch(this.SelectingNumber){
				case 0:
					this.SelectingCharacterControllerInstance.TapStartButton();
					this.Cursor.SetActive(false);
					break;
				case 1:
					this.SelectingCharacterControllerInstance.TapSelectButton(false);
					break;
				case 2:
					this.SelectingCharacterControllerInstance.TapSelectButton(true);
					break;
				case 3:
					if(GameManager.Instance._gameData.RouletteCount <= 0){
							this.SelectingCharacterControllerInstance.TapRouletteButton();
							this.Cursor.SetActive(false);
					}else{
						if(GameManager.Instance.ModifyCoinAmount(-500, true, false)){
							GameManager.Instance.ModifyCoinAmount(500, true, false);
							this.SelectingCharacterControllerInstance.TapRouletteButton();
							this.Cursor.SetActive(false);
						}else{	
							this.SelectingCharacterControllerInstance.CharaNameText.text = LanguageManager.Instance.GetSystemText("Not enough");
							this.SelectingCharacterControllerInstance.ManipulateButtons(true, false, false);
						}
					}
					break;
			}
		}
	}
}
