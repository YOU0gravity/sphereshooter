﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// メニュー画面のアイコンの表示・非表示やアニメーションをコントロールする
/// </summary>
public class MenuIconController : MonoBehaviour {
	[SerializeField] private GameObject[] Icons;
	[SerializeField] private GameObject TitleLogo;

	private bool IsIconsAppearing;
	private bool IsIconsDisappearing;
	private float EnlargingIconScale;


	private bool IsLogoLerping;
	private Vector3 EndPos;
	private Vector3 StartPos;
	private float EndTime = 0.3f;
	private float StartTime;

	// Use this for initialization
	void Start () {
		this.ManipulateLogoAppearance(true);
		this.ManipulateIconsAppearance(true);
	}
	
	// Update is called once per frame
	void Update () {
		if(this.IsLogoLerping){
			var diff = Time.timeSinceLevelLoad - this.StartTime;
			if (diff > this.EndTime) {
				this.TitleLogo.transform.localPosition = this.EndPos;
				this.IsLogoLerping = false;
			}
			var rate = diff / this.EndTime;
			this.TitleLogo.transform.localPosition = Vector3.Lerp(this.StartPos, this.EndPos, rate);
		}

		if(this.IsIconsAppearing || this.IsIconsDisappearing){
			foreach(GameObject icon in this.Icons){
				if(this.IsIconsAppearing){
					this.EnlargingIconScale += Time.deltaTime;
					if(this.EnlargingIconScale > 1){
						this.EnlargingIconScale = 1;
						this.IsIconsAppearing = false;
					}
				}

				if(this.IsIconsDisappearing){
					this.EnlargingIconScale -= Time.deltaTime;
					if(this.EnlargingIconScale < 0){
						this.EnlargingIconScale = 0;
						this.IsIconsDisappearing = false;
					}
				}

				icon.transform.localScale = Vector3.one * this.EnlargingIconScale;
			}
		}

		if(Input.GetKeyDown(KeyCode.P)){
			this.ManipulateLogoAppearance(true);
			this.ManipulateIconsAppearance(true);
		}
	}

	public void Show(int num){
		if(num == 6){
			this.TitleLogo.SetActive(true);
		}else{
			if(num >= 0 && num < 4)
				this.Icons[num].SetActive(true);

			if(num == 5)
				foreach(GameObject icon in this.Icons)
					icon.SetActive(true);
		}
	}
	public void Hide(int num){
		if(num == 6){
			this.TitleLogo.SetActive(false);
		}else{
			if(num >= 0 && num < 4)
				this.Icons[num].SetActive(false);

			if(num == 5)
				foreach(GameObject icon in this.Icons)
					icon.SetActive(false);
		}
	}


	private void ManipulateLogoAppearance(bool isAppearing){
		this.EndPos = this.TitleLogo.transform.localPosition;
		if(isAppearing){
			this.EndPos.x = 0;
		}else{
			this.EndPos.x = -20;
		}
		this.StartPos = this.TitleLogo.transform.localPosition;
		this.StartTime = Time.timeSinceLevelLoad;
		this.IsLogoLerping = true;
	}
	private void ManipulateIconsAppearance(bool isAppearing){
		if(isAppearing){
			this.IsIconsAppearing = true;
			this.EnlargingIconScale = 0;
		}else{
			this.IsIconsDisappearing = true;
			this.EnlargingIconScale = 1;
		}
	}

	public void EnlargeOneIcon(int num){
		foreach(GameObject icon in this.Icons)
			icon.transform.localScale = new Vector3(1, 1, 1);

		if(num == -1) return;

		this.Icons[num].transform.localScale = new Vector3(1.3f, 1.3f, 1.3f);
	}
}
