using UnityEngine;
using System;
using System.Collections;

/// <summary>
/// Dimensionが変化したときEnabledになり、
/// LerpとSlerpを用いて、なめらかにカメラを移動します
/// </summary>
public class SmoothCameraMover : MonoBehaviour
{
	public readonly Vector3 CameraPos3D = new Vector3(0, 10, -25);
	public readonly Vector3 CameraPos2DWest = new Vector3(-50, 1, -20);
	public readonly Vector3 CameraPos2DNorth = new Vector3(0, 30, 15);
	public readonly Vector3 CameraPos2DEast = new Vector3(50, 1, 13);
	public readonly Vector3 CameraPos2DSouth = new Vector3(0, -30, 10);

	[SerializeField] private DimensionChangingIndicatorAnimator _dimensionChangingIndicatorAnimator;

	float time = 1;
	Vector3	endPosition;
	Quaternion endRotation;
	private float startTime;
	private Vector3 startPosition;
	private Quaternion startRotation;
	private int DimensionNumber;
	public Camera Cam;

	private bool IsLerpingSize;

	void OnEnable ()
	{
		if (time <= 0) {
			transform.position = endPosition;
			transform.rotation = endRotation;
			enabled = false;
			return;
		}

		startTime = Time.timeSinceLevelLoad;
		startPosition = transform.position;
		startRotation = transform.rotation;

		_dimensionChangingIndicatorAnimator.Sprite.enabled = true;
	}

	
	void Update ()
	{
		var diff = Time.timeSinceLevelLoad - startTime;
		if (diff > time) {
			transform.position = endPosition;
			transform.rotation = endRotation;
			enabled = false;
			switch(this.DimensionNumber){
				case 0://3D
					break;
				case 1: case 2: case 3:
					this.Cam.orthographic = true;
					break;
			}

			_dimensionChangingIndicatorAnimator.Sprite.enabled = false;
			GameManager.Instance.OnDimensionChanged();
			return;
		}

		var rate = diff / time;
		
		transform.position = Vector3.Lerp (startPosition, endPosition, rate);
		transform.rotation = Quaternion.Slerp(startRotation, endRotation, rate);

		if(this.IsLerpingSize){
			this.Cam.orthographicSize += Time.deltaTime * 10;
			if(this.Cam.orthographicSize > 30){
				this.Cam.orthographicSize = 30;
				this.IsLerpingSize = false;
			}
		}
	}

	void OnDrawGizmosSelected ()
	{
#if UNITY_EDITOR

		if( !UnityEditor.EditorApplication.isPlaying || enabled == false ){
			startPosition = transform.position;
		}

		UnityEditor.Handles.Label(endPosition, endPosition.ToString());
		UnityEditor.Handles.Label(startPosition, startPosition.ToString());
#endif
		Gizmos.DrawSphere (endPosition, 0.1f);
		Gizmos.DrawSphere (startPosition, 0.1f);

		Gizmos.DrawLine (startPosition, endPosition);
	}


	//Dimensionが変化するときに呼ばれる
	public void ChangeDimensions(int dimensionNumber){
		this.DimensionNumber = dimensionNumber;
		Vector3 worldContainerPos = GameManager.PlayerWorldPosContainer.transform.localPosition;
		switch(this.DimensionNumber){
			case 0://3D
				Vector3 localContainerPos = GameManager.PlayerLocalPosContainer.transform.localPosition;
				this.Cam.orthographic = false;
				endPosition = worldContainerPos + localContainerPos + CameraPos3D;
				endRotation = Quaternion.Euler(new Vector3(30,0,0));
				break;
			case 1://W
				this.IsLerpingSize = true;
				endPosition = worldContainerPos + CameraPos2DWest;
				endRotation = Quaternion.Euler(new Vector3(0,90,0));
				break;
			case 2://N
				this.Cam.orthographicSize = 17;
				endPosition = worldContainerPos + CameraPos2DNorth;
				endRotation = Quaternion.Euler(new Vector3(90,0,0));
				break;
			case 3://E
				this.IsLerpingSize = true;
				endPosition = worldContainerPos + CameraPos2DEast;
				endRotation = Quaternion.Euler(new Vector3(0,-90,0));
				break;
			case 4://S
				this.Cam.orthographicSize = 17;
				endPosition = worldContainerPos + CameraPos2DSouth;
				endRotation = Quaternion.Euler(new Vector3(-90,0,0));
				break;
		}
		enabled = true;
	}
}