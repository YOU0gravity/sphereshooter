﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// チュートリアル用の、視点変更を説明するための四角オブジェクトをコントロールする
/// </summary>
public class MiniCubeController : MonoBehaviour {
	private int StateNumber;
	private Quaternion GridEndRot;
	private float TimeRecord;
	[SerializeField] private float Speed = 50f;


	// Update is called once per frame
	void Update () {
		Debug.Log(StateNumber);
		TimeRecord += Time.unscaledDeltaTime * Speed;
		Quaternion EndRot = Quaternion.identity;
		switch(this.StateNumber){
			case 0:
				EndRot = Quaternion.Euler(TimeRecord, 0, 0);
				break;
			case 1:
				EndRot = Quaternion.Euler(90 - TimeRecord, TimeRecord, TimeRecord);
				break;
			case 2:
				EndRot = Quaternion.Euler(TimeRecord, 90 - TimeRecord, 90 - TimeRecord);
				break;
			case 3:
				EndRot = Quaternion.Euler(90 - TimeRecord, 0, 0);
				break;
		}
		if(TimeRecord > 0){
			transform.localRotation = EndRot;

			if(TimeRecord > 90){
				TimeRecord = -20;
				this.SetGrid();
				this.StateNumber++;
				if(this.StateNumber > 3)
					this.StateNumber = 0;
			}
		}
	}
	//90 0 0
	//0 90 90
	//90 0 0
	//0 0 0

	private void SetGrid(){
		switch(this.StateNumber){
			case 0:
				GridEndRot = Quaternion.Euler(90, 0, 0);
				break;
			case 1:
				GridEndRot = Quaternion.Euler(0, 90, 90);
				break;
			case 2:
				GridEndRot = Quaternion.Euler(90, 0, 0);
				break;
			case 3:
				GridEndRot = Quaternion.Euler(0, 0, 0);
				break;
		}
		transform.localRotation = GridEndRot;
	}
}
