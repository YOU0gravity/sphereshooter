﻿using UnityEngine;
using System.Collections;

/// <summary>
/// プレイヤーから放たれる銃弾の情報を持ち、
/// Dimensionの変化時の挙動などをコントロールします
/// </summary>
public class BulletController : MonoBehaviour {
	private float WeaponSpeed{ get; set; }//銃弾の速さ
	private float WeaponDistance { get; set; }//銃弾が進む距離
	public float WeaponAttack { get; set; }//銃弾の攻撃力
	private int[] Enchant { get; set; }//銃弾の特殊能力

	[SerializeField] private bool IsRotating = true;
	[SerializeField] private GameObject Model;
	private float OriginPosZ;
	private BoxCollider Collider;

	//Enchant
	private int Penetration;

	void Start () {
		this.OriginPosZ = transform.localPosition.z;
		this.Collider = GetComponent<BoxCollider>();
	}
	
	// Update is called once per frame
	void Update () {
		if(!GameManager._smoothCameraMover.enabled)
			transform.Translate(Vector3.forward * this.WeaponSpeed * Time.deltaTime, Space.Self);

		if(transform.localPosition.z - this.OriginPosZ > this.WeaponDistance)
			Destroy(gameObject);
		
		if(this.IsRotating && this.Model != null)
			this.Model.transform.Rotate(new Vector3(0, 0, 400) * Time.deltaTime);		
	}

	public void SetParameter(CharacterInfo instance){
		this.WeaponSpeed = instance.WeaponSpeed;
		this.WeaponDistance = instance.WeaponDistance;
		this.WeaponAttack = instance.WeaponAttack;
		this.Enchant = instance.Enchant;

		for(int i=0; i<3; i++){
			int enchant = this.Enchant[i];
			int enchantID = enchant / 10;
			int enchantLV = enchant % 10;
			switch(enchantID){
				case 11: this.Penetration = enchantLV; break;
			}
		}

		if(this.Collider == null)
			this.Collider = GetComponent<BoxCollider>();


		this.ChangeCollider(GameManager.DimensionNumber);
	}
	public void Collide(){
		if(this.Penetration > 0){
			this.Penetration--;
		}else{
			Destroy(gameObject);
		}
	}

	public void ChangeDimensions(int dimensionNumber){
		this.ChangeCollider(dimensionNumber);
	}
	private void ChangeCollider(int dimensionNumber){
		switch(GameManager.DimensionNumber){
			case 0:
				this.Collider.size = new Vector3(1,1,1);
				break;
			case 3:
				this.Collider.size = new Vector3(40,1,1);
				break;
			case 2: 
				this.Collider.size = new Vector3(1,40,1);
				break;
		}
	}
}
