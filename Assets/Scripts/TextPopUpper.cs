﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// コイン取得時に、取得したコインの枚数を表示するテキストをポップアップさせるためのクラス
/// </summary>
public class TextPopUpper : MonoBehaviour {
	private RectTransform RectTrans;
	private Vector3 OriginPos;
	private float AddValue;
	private bool IsPopUpping;
	private float PopUpSpeed = 7f;
	private int AlphaReductionCoef = 8;
	private Text CoinText;
	private int HowMuch;
	private int AlphaValue;

	// Use this for initialization
	void Start () {
		this.RectTrans = GetComponent<RectTransform>();
		this.OriginPos = this.RectTrans.localPosition;
		this.AddValue = 0;
		this.CoinText = GetComponent<Text>();
		this.CoinText.text = "";
	}
	
	// Update is called once per frame
	void Update () {
		if(this.IsPopUpping){
			this.AddValue += this.PopUpSpeed;
			this.RectTrans.localPosition = this.OriginPos + new Vector3(0, this.AddValue, 0);

			if(this.AlphaValue > 0)
				this.AlphaValue -= this.AlphaReductionCoef;

			if(this.AlphaValue <= 0)
				this.AlphaValue = 0;

			string alphaValueString = this.AlphaValue.ToString("X2");
			string howMuchText = "<color=#FFE732"+ alphaValueString +">+" + this.HowMuch.ToString() + "</color>";
			if(this.HowMuch < 0){
				howMuchText = "<color=#8D22C1" + alphaValueString + ">" + this.HowMuch.ToString() + "</color>";
			}else if(this.HowMuch == 0){
				howMuchText = "";
			}
			this.CoinText.text = howMuchText;
		}
	}

	public IEnumerator PopUp(int howMuch){
		this.OriginPos = this.RectTrans.localPosition + Vector3.up * 10;
		this.AddValue = 0;
		this.AlphaValue = 255;
		this.IsPopUpping = true;
		this.HowMuch = howMuch;

		yield return new WaitForSeconds(1f);

		this.IsPopUpping = false;
		this.CoinText.text = "";
	}
}
