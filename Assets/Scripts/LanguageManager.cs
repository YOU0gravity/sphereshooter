﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// 言語を切り替えるクラス
/// </summary>
public class LanguageManager : MonoBehaviour {
	public static int LanguageID = 1; //0:English, 1:Japanese
	private static readonly Dictionary<string, int> SystemTextDic = new Dictionary<string, int>(){
		{"Game", 0}, {"Start!", 1}, {"Tutorial", 2}, {"Tutorial completed!", 3}, {"Now, go have a good time!", 4}, {"High Score", 5},
		{"New Score", 6}, {"Tap to return", 7}, {"Cost 500 stars", 8}, {"First time", 9}, {"Not enough", 10},
		{"Got", 11}, {"Already have", 12}, {"Return confirm", 13}, {"Your Score", 14}
	};
	private static readonly string[,] SystemText = new string[,]{
		//English
		{
			"Game", "Start!",  "Tutorial", "Tutorial completed!", "Now, go have a good time!", "Top score is ",
			"New record!\nNow,\n top score is ","Tap to return to title", "Tap to stop\n(cost 500 stars)", "Tap to stop", "Need 500 stars",
			"You've got", "You already have", "Are you sure you want to return the top?\n(You can't get stars in current level)", "Your score is "
		},

		//日本語
		{
			"ゲーム", "スタート！", "チュートリアル", "チュートリアル終了！", "本番が始まります", "今までの最高記録: ",
			"新記録！新しい最高記録: ", "タップでメニューへ戻ります", "タップでストップ！\n(星500消費)", "タップでストップ！", "星500必要です",
			"新キャラを入手！", "すでに入手済みでした", "トップ画面へ戻りますか？\n（今回入手した星は消えます）","今回の記録: "
		}
	};
	private static readonly string[,] NameText = new string[,]{
		//English
		{
			"Space satellite", "Extraterrestial", "Big Fish", "Hand of Sushi Shokunin", "Octopus", "Singing bird", "Balloon kid",
			"Paperplane", "Hero", "Skyboarder", "Crow", "Flying pig", "Magical girl", "Ninja"
		},

		//日本語
		{
			"人工衛星", "地球外生命体", "サカナさん", "寿司職人の手", "タコさん", "インコさん", "風船で飛んでいる子供",
			"紙飛行機", "ヒーロー", "スカイボーダー", "カラスさん", "飛べるブタさん", "魔法少女", "ニンジャ"
		}
	};

	//Singleton Instance
	private static LanguageManager instance;
	public static LanguageManager Instance {
		private set { instance = value; }
		get{ return instance; }
	}

	void Awake() {
		if(instance == null){
			instance = this;
		}else{
			Destroy(gameObject);
		}

		LanguageID = 1;//Ja
		#if UNITY_ANDROID
			if (Application.systemLanguage != SystemLanguage.Japanese){
				LanguageID = 0;//En
			}
		#endif
	}

	public string GetSystemText(string key)
	{
		return SystemText[LanguageID, SystemTextDic[key]];
	}
	public string GetNameText(int characterID)
	{
		return  NameText[LanguageID, characterID];
	}
}
