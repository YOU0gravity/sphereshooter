﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// チュートリアル時などに表示される、タッチを誘導するアイコンをアニメーションさせるためのクラス
/// </summary>
public class TouchIconAnimationController : MonoBehaviour {

	private bool IsFlicking;
	private bool IsTapping;
	private Vector3 StartLocalPos;
	private Vector3 EndLocalPos;
	private float LerpStartTime;
	private readonly float LerpingTime = 0.2f;
	private bool TowardRight;
	private float WaitingTime;
	[SerializeField] private ParticleSystem TapEffect;

	private bool TapToggle;


	// Update is called once per frame
	void Update () {
		if(this.IsFlicking){
			var diff = Time.timeSinceLevelLoad - LerpStartTime;
			if (diff > LerpingTime) {
				WaitingTime += Time.deltaTime;

				if(WaitingTime < 1f){
					transform.localPosition = EndLocalPos;
					if(WaitingTime > 0.2f)
						transform.rotation = Quaternion.Euler(-30,0,0);
				}else{
					TowardRight = !TowardRight;
					LerpStartTime = Time.timeSinceLevelLoad;
					if(TowardRight){
						StartFlickAnimation(transform.localPosition, new Vector3(2, 0, 12));
					}else{
						StartFlickAnimation(transform.localPosition, new Vector3(-2, 0, 12));
					}
				}
				return;
			}

			var rate = diff / LerpingTime;
			transform.localPosition = Vector3.Lerp(StartLocalPos, EndLocalPos, rate);

			this.TapEffect.transform.position = transform.position;
			this.TapEffect.Emit(1);
		}

		if(this.IsTapping){
			if(WaitingTime > 0.8f){
				if(TapToggle){
					transform.rotation = Quaternion.Euler(60,0,0);
					EffectManager.Instance.ApplyEffect("Roulette");
				}else{
					transform.rotation = Quaternion.Euler(-30,0,0);
				}
				TapToggle = !TapToggle;
				WaitingTime = 0;
			}
			WaitingTime += Time.unscaledDeltaTime;
		}
	}
	public void StartFlicking(){
		this.IsFlicking = true;
		this.StartFlickAnimation(transform.localPosition, new Vector3(-2, 0, 12));
	}
	public void StartTapping(){
		this.IsTapping = true;
		WaitingTime = 0.9f;
	}
	public void Disactivate(){
		WaitingTime = 0;
		this.IsFlicking = false;
		this.IsTapping = false;
		transform.localPosition = new Vector3(0, 0, 12);
	}
	private void StartFlickAnimation(Vector3 startPos, Vector3 endPos){
		StartLocalPos = startPos;
		EndLocalPos = endPos;
		LerpStartTime = Time.timeSinceLevelLoad;
		WaitingTime = 0;
		transform.rotation = Quaternion.Euler(60,0,0);
		EffectManager.Instance.ApplyEffect("Roulette");
	}
}
