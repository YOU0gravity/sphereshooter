﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// プレイヤーが触れた際に効果を発揮するアイテムなどを操るクラス
/// </summary>
public class ItemController : MonoBehaviour {
	public int ItemID;
	private float RotNum;
	void Update () {
		if(this.ItemID == 0){
			this.RotNum += Time.deltaTime * 150;
			if(this.RotNum > 360)
				this.RotNum = this.RotNum % 360;
			
			switch(GameManager.DimensionNumber){
				case 0: case 2:
					transform.localRotation = Quaternion.Euler(0,this.RotNum,0);
					break;
				case 1: case 3:
					transform.localRotation = Quaternion.Euler(this.RotNum,0,90);
					break;
			}
		}
	}


	public void OnTriggerEnter(Collider other){
		if(other.gameObject.tag == "Player"){
			switch(this.ItemID){
				case 0://DimensionArrow
					if(GameManager._smoothCameraMover.enabled == false && Time.timeSinceLevelLoad - GameManager.DimensionChangedTime > 0.5f){
						int currentDimensionNumber = GameManager.DimensionNumber;
						int toDimensionNumber = 0;
						if(this.RotNum < 60 || this.RotNum >= 300){//上
							toDimensionNumber = 2;
							if(currentDimensionNumber == 2)
								toDimensionNumber = 0;
		
						}else if(this.RotNum >= 60 && this.RotNum < 180){//右
							toDimensionNumber = 3;
							if(currentDimensionNumber == 3)
								toDimensionNumber = 0;

						}else if(this.RotNum >= 180 && this.RotNum < 300){//左
							toDimensionNumber = 1;
							if(currentDimensionNumber == 1)
								toDimensionNumber = 0;
						}
					
						GameManager.Instance.ChangeDimensions(toDimensionNumber);
					}

					Destroy(gameObject);
					break;

				case 1://CheckPoint
					StageManager.Instance.CreateNextRoad();
					Destroy(gameObject);
					break;
				
				case 2://GoalPoint
					// GameManager.Instance.EndTutorial();
					GameManager.Instance.DidPassGoalForTutorial();
					break;
			}
		}
	}
}
