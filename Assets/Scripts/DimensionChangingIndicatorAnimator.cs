﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Dimension変化時に表示されるアイコンをアニメーションさせます
/// </summary>
public class DimensionChangingIndicatorAnimator : MonoBehaviour {
	private Vector3 OriginScale;
	public SpriteRenderer Sprite;

	private float TimeForExpansion;
	private bool IsExpansioning;
	void Start(){
		OriginScale = transform.localScale;
		Sprite = GetComponent<SpriteRenderer>();
	}

	void Update(){
		if(this.Sprite.enabled){
			transform.Rotate(new Vector3(0, 0, -180) * Time.deltaTime);

			if(this.IsExpansioning){
				this.TimeForExpansion += Time.deltaTime / 4f;
			}else{
				this.TimeForExpansion -= Time.deltaTime / 4f;
			}
			
			if(Mathf.Abs(this.TimeForExpansion) > 0.1f){
				this.IsExpansioning = !this.IsExpansioning;
			}
			transform.localScale = this.OriginScale + new Vector3(this.TimeForExpansion, this.TimeForExpansion, 0);
		}
	}
}
