﻿using UnityEngine;
using System.Collections;

/// <summary>
/// プレイヤーが移動可能であることを示す、四角いブロックの動きをコントロールする
/// </summary>
public class MovableBlocksController : MonoBehaviour
{

	float time = 1;
	private float startTime;
	private int CurrentDimensionNumber;
	private int BeforeDimensionNumber = 2;
	[SerializeField] private GameObject[] Blocks;

	[SerializeField] private Material[] Mats;


	void OnEnable ()
	{
		if (time <= 0) {
			for(int i=0; i<9; i++)
				this.Blocks[i].transform.localPosition = this.BlocksPos(i, this.CurrentDimensionNumber);
			
			enabled = false;
			return;
		}
		startTime = Time.timeSinceLevelLoad;
	}

	void Update ()
	{
		var diff = Time.timeSinceLevelLoad - startTime;
		if (diff > time) {
			for(int i=0; i<9; i++)
				this.Blocks[i].transform.localPosition = this.BlocksPos(i, this.CurrentDimensionNumber);
			
			enabled = false;
		}

		var rate = diff / time;
		for(int i=0; i<9; i++){
			Vector3 startPos = this.BlocksPos(i, this.BeforeDimensionNumber);
			Vector3 endPos = this.BlocksPos(i, this.CurrentDimensionNumber);
			this.Blocks[i].transform.localPosition = Vector3.Lerp(startPos, endPos, rate);
		}
	}
	public void ChangeDimensions(int dimensionNumber){
		startTime = Time.timeSinceLevelLoad;
		this.BeforeDimensionNumber = this.CurrentDimensionNumber;
		this.CurrentDimensionNumber = dimensionNumber;
		enabled = true;
	}

	//次元に応じて、9つのブロックがそれぞれどこに移動すべきなのかを計算して返す
	private Vector3 BlocksPos(int blockNumber, int dimensionNumber){
		Vector3 blocksPos = Vector3.zero;
		switch(dimensionNumber){
			case 0:
				blocksPos = new Vector3(-5 + (5 * (blockNumber % 3)), 5 - 5 * (blockNumber / 3), 0);
				break;
			case 1: case 3:
				blocksPos = new Vector3(0, 5 - 5 * (blockNumber / 3), 0);
				break;
			case 2: case 4:
				blocksPos = new Vector3(-5 + (5 * (blockNumber % 3)), 0, 0);
				break;
		}
		return blocksPos;
	}

	public void ChangeColor(int colorNum){
		Material changingMat = this.Mats[colorNum];
		for(int i=0; i<9; i++){
			this.Blocks[i].GetComponent<MeshRenderer>().material = changingMat;
		}
	}
}