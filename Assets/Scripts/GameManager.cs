﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(LanguageManager))]
[RequireComponent(typeof(StageManager))]
[RequireComponent(typeof(EffectManager))]
public class GameManager : MonoBehaviour {
	//Player
	public static GameObject PlayerWorldPosContainer;
	public static GameObject PlayerLocalPosContainer;
	public static GameObject Player3DModel;
	private int TresureHunter;


	//Game State
	[System.NonSerialized] public bool IsLogoAnimationEnded;
	[SerializeField] private GameObject CameraParent;
	public static int DimensionNumber;	
	public static GameObject PlayerFollowingContainer;
	private bool CanOperate = true;
	private bool IsLevelFinished = false;
	private int RemainingLives = 1;
	private float SpeedUpCoef = 0.2f;
	[System.NonSerialized] public bool IsInvincible = false;
	public static float DimensionChangedTime;
	public static bool IsStageStarted;
	public CharacterInfo SelectedChara;
	[System.NonSerialized] public bool IsTutorial;
	[System.NonSerialized] public bool PressedStartButton;
	private float GameSpeedThreshold;
	private float CurrentGameSpeed;
	private bool IsGameEnded;
	private bool IsDebugMode = false;
	private bool IsTowardNorth;
	private int AccumulatedPickedUpCoin;
	private int AccumulatedEnemyCoin;
	private float LastPickedUpCoinTime;
	private float LastEnemyCoinTime;


	//UI
	[SerializeField] private Text[] PoppingUpTexts;
	private int PoppingUpTextCount;
	[SerializeField] private GameObject MoneyImage;
	[SerializeField] private Text MoneyText;
	[SerializeField] private Text GameStartText;
	[System.NonSerialized] public bool IsPausing = false;
	[SerializeField] private GameObject PausePanel;
	[SerializeField] Text ConfirmText;
	private string ConfirmingCommand;
	[SerializeField] private GameObject ConfirmingMenu;
	[SerializeField] private GameObject PauseMenu;
	[SerializeField] private GameObject ResultMenu;
	[SerializeField] private Text PointText;
	[SerializeField] private GameObject MovableBlocksParent;
	public GameObject[] MenuButtons;



	//Scripts
	private PlayerController _playerController;
	private GameOption _gameOptionInstance;
	public SelectingCharacterController _selectingCharacterController;
	public GameData _gameData;
	public CharacterLockManager _characterLockManager;
	private CameraShaker _cameraShaker;
	public static SmoothCameraMover _smoothCameraMover;
	[System.NonSerialized] public TutorialManager _tutorialManager;



	//Records
	private int DestroyedEnemyCount;
	[System.NonSerialized] public int CurrentShootCount;
	[System.NonSerialized] public CharacterInfo[] CharacterInfoArray;
	public static readonly int TotalCharacterNumber = 14;

	//Singleton Instance
	private static GameManager instance;
	public static GameManager Instance {
		private set { instance = value; }
		get{ return instance; }
	}


	void Awake(){
		if(instance == null){
			instance= this;
		}else{
			Destroy(gameObject);
		}

		DimensionNumber = 2;
		IsStageStarted = false;


		PressedStartButton = false;

		_gameData = new GameData();
		_characterLockManager = new CharacterLockManager();
		CharacterInfoArray = new CharacterInfo[TotalCharacterNumber];

		_tutorialManager = GetComponent<TutorialManager>();
		_tutorialManager.enabled = false;

		_gameData.ApplicationStartUpCount++;

		/*
			START DEBUGGING CODE
		 */
		// _gameData.ApplicationStartUpCount = 1;
		// LanguageID = 0;
		/*
			END DEBUGGING CODE
		*/

		if(_gameData.ApplicationStartUpCount == 1){//初回起動時にチュートリアルを起動
			if(_characterLockManager.UnlockedNumber[0] <= 0)
				_characterLockManager.UnlockedNumber[0] = 1;
			_characterLockManager.Save();
			IsTutorial = true;
			for(int i=1; i<4; i++){
				MenuButtons[i].SetActive(false);
			}
			_selectingCharacterController.TutorialButton.SetActive(false);

			_tutorialManager.enabled = true;
			_tutorialManager.PrepareTutorial();

		}else{
			StageData.Instance.CreateRoadsInfo();
			IsTutorial = false;
			if(_gameData.RouletteCount == 0){
				MenuButtons[1].SetActive(false);
				MenuButtons[2].SetActive(false);
			}
		}
		

		SpeedUpCoef = StageData.Instance.SpeedUpCoef;

		MoneyText.text = _gameData.CurrentKeepingCoin.ToString();

		IsGameEnded = true;


		//TODO: 別のファイルに記述する（数が少ないうちはここに書く）
		for(int i=0; i<TotalCharacterNumber; i++)
			CharacterInfoArray[i] = new CharacterInfo();

		//PlayerID, Background, ShootOffset, WeaponSpeed, WeaponDistance, WeaponAttack, Enchant * 3
		CharacterInfoArray[0].RegisterNewCharacter(0,0,0.2f,30,30,20,0,0,0);//Satellite,  Offset+, Attack+
		CharacterInfoArray[1].RegisterNewCharacter(1,0,0.1f,45,15,10,0,0,0);//ET, Offset++, Speed+, Distance-
		CharacterInfoArray[2].RegisterNewCharacter(2,1,0.2f,15,30,10,112,0,0);//Fish, Offset+, Speed-, Penetration++,
		CharacterInfoArray[3].RegisterNewCharacter(3,1,0.2f,15,45,10,111,0,0);//Sushi, Offset+, Speed-, Distance+, Penetration+
		CharacterInfoArray[4].RegisterNewCharacter(4,1,0.3f,45,45,10,0,0,0);//Octopus, Speed+, Distance+
		CharacterInfoArray[5].RegisterNewCharacter(5,2,0.2f,30,30,10,111,0);//Bird, Offset+, Penetration+
		CharacterInfoArray[6].RegisterNewCharacter(6,2,0.2f,30,45,10,0,0,0);//Balloon, Offset+, Distance+
		CharacterInfoArray[7].RegisterNewCharacter(7,2,0.1f,30,15,10,111,0,0);//Paperplane, Offset++, Distance-, Penetration+
		CharacterInfoArray[8].RegisterNewCharacter(8,2,0.2f,45,30,10,0,0,0);//Hero, Offset+, Speed+
		CharacterInfoArray[9].RegisterNewCharacter(9,2,0.1f,30,30,10,0,0,0);//Skyboarder, Offset++
		CharacterInfoArray[10].RegisterNewCharacter(10,2,0.2f,15,45,10,111,0,0);//Crow, Offset+, Speed-, Distance+ Penetration+
		CharacterInfoArray[11].RegisterNewCharacter(11,2,0.3f,45,45,10,0,0,0);//Flying pig Distance+, Speed+
		CharacterInfoArray[12].RegisterNewCharacter(12,2,0.3f,30,30,10,112,0,0);//Magical girl, Penetration++
		CharacterInfoArray[13].RegisterNewCharacter(13,2,0.2f,45,15,10,111,0,0);//Ninja Offset+, Speed+, Distance-, Penetration+
	}

	// Use this for initialization
	void Start () {
		PlayerFollowingContainer = GameObject.Find("PlayerFollowingContainer");

		PlayerWorldPosContainer = Instantiate(Resources.Load("PlayerWorldPosContainer"), new Vector3(0,0,-20), Quaternion.identity) as GameObject;
		PlayerLocalPosContainer = PlayerWorldPosContainer.transform.GetChild(0).gameObject;

		_playerController = PlayerLocalPosContainer.GetComponent<PlayerController>();

		_cameraShaker = CameraParent.GetComponent<CameraShaker>();
		_smoothCameraMover = CameraParent.GetComponent<SmoothCameraMover>();
		_smoothCameraMover.Cam = CameraParent.GetComponentInChildren<Camera>();
	}

	void Update () {
		if(IsStageStarted){
			if(!_smoothCameraMover.enabled)
				GameSpeedThreshold += Time.deltaTime;


			if(!IsTutorial){
				if(GameSpeedThreshold > 1f){
					GameSpeedThreshold = 0;
					CurrentGameSpeed += SpeedUpCoef;

					if(_playerController.MoveSpeed != 0)
						_playerController.MoveSpeed = CurrentGameSpeed;
					
					EffectManager.Instance.SetPitch(0.7f + CurrentGameSpeed * 0.03f);
				}
			}
		}
		
		//次元移動中はプレイヤーを動かさない
		if(!_smoothCameraMover.enabled){
			PlayerWorldPosContainer.transform.Translate(Vector3.forward * _playerController.MoveSpeed * Time.deltaTime);
		}

		if(IsDebugMode){
			if(Input.GetKeyDown(KeyCode.Alpha1)){
				ChangeDimensions(0);
			}else if(Input.GetKeyDown(KeyCode.Alpha2)){
				ChangeDimensions(2);
			}else if(Input.GetKeyDown(KeyCode.Alpha3)){
				ChangeDimensions(3);
			}else if(Input.GetKeyDown(KeyCode.Alpha4)){
				SelectedChara.ShootOffset /= 2;
				_playerController.SetParameter(SelectedChara);
			}else if(Input.GetKeyDown(KeyCode.Alpha5)){
				SelectedChara.ShootOffset *= 2;
				_playerController.SetParameter(SelectedChara);
			}
			if(Input.GetKeyDown(KeyCode.F)){
				BeingDestroyed();
			}
		}
		
	}

	void LateUpdate(){
		if(!IsLevelFinished){
			Vector3 containerPos = PlayerWorldPosContainer.transform.localPosition;
			Vector3 localContainerPos = PlayerLocalPosContainer.transform.localPosition;

			if(_smoothCameraMover.enabled){
				//Do nothing
			}else{
				switch(DimensionNumber){
					case 0://3D
						Vector3 playerPos = PlayerLocalPosContainer.transform.localPosition;
						CameraParent.transform.position = playerPos + containerPos + _smoothCameraMover.CameraPos3D;
						break;
					case 1://W
						CameraParent.transform.position =  containerPos + _smoothCameraMover.CameraPos2DWest;
						break;
					case 2://N
						CameraParent.transform.position =  containerPos +  _smoothCameraMover.CameraPos2DNorth;
						break;
					case 3://E
						CameraParent.transform.position =  containerPos + _smoothCameraMover.CameraPos2DEast;
						break;
					case 4://S
						CameraParent.transform.position =  containerPos + _smoothCameraMover.CameraPos2DSouth;
						break;
				}
			}
			PlayerFollowingContainer.transform.position = new Vector3(0, 0, containerPos.z);
			
		}
	}

	public void ChooseCharacter(bool isPlus){
		bool CanUseCharacter = false;
		while(!CanUseCharacter){
			if(isPlus){
				_gameData.SelectedCharaID++;
				if(_gameData.SelectedCharaID >= TotalCharacterNumber)
					_gameData.SelectedCharaID = 0;
			}else{
				_gameData.SelectedCharaID--;
				if(_gameData.SelectedCharaID < 0)
					_gameData.SelectedCharaID = TotalCharacterNumber - 1;	
			}

			if(_characterLockManager.UnlockedNumber[_gameData.SelectedCharaID] != 0)
				CanUseCharacter = true;
			
		}
		SelectedChara = CharacterInfoArray[_gameData.SelectedCharaID];

		_selectingCharacterController.SendMessage("ChooseCharacter", _gameData.SelectedCharaID);
	}

	//次元を変えたいとき、このメソッドを呼び出す
	public void ChangeDimensions(int dimensionNumber = 0){
		if(_smoothCameraMover.enabled) return;

		DimensionNumber = dimensionNumber;
		_smoothCameraMover.ChangeDimensions(dimensionNumber);

		GameObject[] coins = GameObject.FindGameObjectsWithTag("StarCoin");
		foreach(GameObject coin in coins){
			coin.SendMessage("ChangeDimensions", dimensionNumber);
		}

		GameObject[] obstacles = GameObject.FindGameObjectsWithTag("Obstacles");
		foreach(GameObject obs in obstacles){
			obs.SendMessage("ChangeDimensions", dimensionNumber);
		}

		GameObject[] bullets = GameObject.FindGameObjectsWithTag("Bullet");
		foreach(GameObject b in bullets){
			b.SendMessage("ChangeDimensions", dimensionNumber);
		}

		GameObject[] accessories = GameObject.FindGameObjectsWithTag("Accessory");
		foreach(GameObject a in accessories){
			a.SendMessage("ChangeDimensions", dimensionNumber);
		}

		_playerController.SendMessage("ChangeDimensions", dimensionNumber);
		MovableBlocksParent.SendMessage("ChangeDimensions", dimensionNumber);

		if(dimensionNumber == 0)
			EffectManager.Instance.ApplyEffect("DimensionChange", transform.position);
	}

	//次元が変わった直後に呼び出されるようにする
	public void OnDimensionChanged(){
		_playerController.OnDimensionChanged(DimensionNumber);

		DimensionChangedTime = Time.timeSinceLevelLoad;

		if(DimensionNumber == 0){
			int toDimensionNum = 3;
			if(IsTowardNorth){
				toDimensionNum = 2;
			}
			IsTowardNorth = !IsTowardNorth;
			ChangeDimensions(toDimensionNum);
		}
	}
	
	public void StartStage(){
		StartCoroutine(StartStageCoroutine());
	}
	private IEnumerator StartStageCoroutine(){
		CurrentGameSpeed = _playerController.MoveSpeed;
		EffectManager.Instance.PlayBGM(true);

		PressedStartButton = true;
		
		if(Player3DModel == null){
			SelectedChara = CharacterInfoArray[_gameData.SelectedCharaID];
			GameObject playerModel = Instantiate(Resources.Load("PlayerModels/" + SelectedChara.PlayerID), PlayerWorldPosContainer.transform.position, Quaternion.identity) as GameObject;
			playerModel.transform.parent = PlayerLocalPosContainer.transform;
			Player3DModel = playerModel;
		}else{
			Debug.LogAssertion("3D model is already Instantiated! The name is " + Player3DModel);
		}

		StageManager.Instance.Initialize(SelectedChara.GameBackground, PlayerLocalPosContainer.transform.position.z + 50f, IsTutorial);

		Player3DModel.transform.localRotation = Quaternion.identity;
		_playerController.SetParameter(SelectedChara);

		MovableBlocksParent.GetComponent<MovableBlocksController>().ChangeColor(SelectedChara.GameBackground);
		
		MovableBlocksParent.SetActive(true);
		
		_selectingCharacterController.HideContainer();


		GameStartText.text = LanguageManager.Instance.GetSystemText("Game");
		yield return new WaitForSeconds(1f);
		GameStartText.text = LanguageManager.Instance.GetSystemText("Start!");
		IsStageStarted = true;
		yield return new WaitForSeconds(2f);

		if(IsTutorial){
			_tutorialManager.StartTutorial();
		}else{
			GameStartText.text = "";
		}
		
	}

	public void DidPassGoalForTutorial(){
		_tutorialManager.DidPassGoal();
	}

	public IEnumerator GameEnd(bool doesReachGoal){
		PlayerLocalPosContainer.SetActive(false);
		EffectManager.Instance.PlayBGM(false);

		bool isHighScore = false;
		int beforeHighScore = _gameData.MaxDestroyPoint;
		if(DestroyedEnemyCount > beforeHighScore){
			_gameData.MaxDestroyPoint = DestroyedEnemyCount;
			isHighScore = true;
		}
		IsGameEnded = false;

		if(StageManager.Instance.CurrentStageLevel > _gameData.MaxStageLevel)
			_gameData.MaxStageLevel = StageManager.Instance.CurrentStageLevel;

		if(!IsDebugMode){
			_gameData.TotalDestroyedEnemyCount += DestroyedEnemyCount;
			_gameData.TotalShootCount += CurrentShootCount;
		}

		_gameData.Save();

		string backText = LanguageManager.Instance.GetSystemText("Your Score") + DestroyedEnemyCount + "\n";
		if(beforeHighScore > 0)
			backText += LanguageManager.Instance.GetSystemText("High Score") + beforeHighScore;

			
		if(isHighScore)
			backText += "\n\n" + LanguageManager.Instance.GetSystemText("New Score") + DestroyedEnemyCount + "!";
			

		backText += "\n\n" + LanguageManager.Instance.GetSystemText("Tap to return");
		
		yield return new WaitForSeconds(2f);
		ResultMenu.SetActive(true);
		StartCoroutine(AnimateText(backText, isHighScore));
	}
	private IEnumerator AnimateText(string text, bool isHighScore = false){
		Text resultText = ResultMenu.GetComponentInChildren<Text>();
		if(text.Length > 0){
			for(int i=0; i<text.Length; i++){
				string viewingText = text.Substring(0,i+1);
				resultText.text = viewingText;
				EffectManager.Instance.ApplyEffect("Key", Vector3.zero);
				yield return new WaitForSecondsRealtime(0.05f);
			}
		}else{
			resultText.text = "";
		}
		if(isHighScore)
			EffectManager.Instance.ApplyEffect("Unlocked", Vector3.zero);

		IsLevelFinished = true;
	}

	public void Pause(){
		if(CanOperate == false) return;

		IsPausing = !IsPausing;
		if(IsPausing){
			Time.timeScale = 0;
			PausePanel.SetActive(true);
		}else{
			Time.timeScale = 1f;
			PausePanel.SetActive(false);
		}
	}

	public void Confirm(string command){
		if(command == "OK"){
			Pause();
			//実行する
			switch(ConfirmingCommand){
				case "GIVEUP":
					SceneManager.LoadScene("Runner");
					break;
			}

		}else if(command == "CANCEL"){
			ConfirmingMenu.SetActive(false);
			PauseMenu.SetActive(true);
			

		}else{
			//確認画面へ移行
			ConfirmingCommand = command;
			switch(ConfirmingCommand){
				case "GIVEUP":
					ConfirmText.text = LanguageManager.Instance.GetSystemText("Return confirm");
					break;

				case "RESULT_OK":
					SceneManager.LoadScene("Runner");
					break;
				case "RESULT_RETRY":
					SceneManager.LoadScene("Runner");
					break;
			}
			ConfirmingMenu.SetActive(true);
			PauseMenu.SetActive(false);
		}
	}


	public void DefeatEnemy(GameObject enemyObject, bool isSelfDestruction = false){
		Vector3 effectPos = enemyObject.transform.position + new Vector3(0,1,0);
		
		EffectManager.Instance.ApplyEffect("Explosion", effectPos);

		if(isSelfDestruction == false){
			EffectManager.Instance.ApplyEffect("ShakeS");
			EffectManager.Instance.ApplyEffect("CoinSpread", effectPos);

			GetCoin(effectPos, enemyObject.GetComponent<EnemyController>().DropMoneyAmount, false);

			if(IsGameEnded){
				DestroyedEnemyCount++;
				PointText.text = DestroyedEnemyCount.ToString();
			}
		}
		Destroy(enemyObject);
	}
	
	//ダメージがあるときはtrueを、ないときはfalseを返す
	public bool Collide(){
		//無敵状態でなければダメージを受ける
		if(!IsInvincible && !_playerController.IsStealthState && !_smoothCameraMover.enabled){
			EffectManager.Instance.ApplyEffect("Damage", Vector3.zero);
			EffectManager.Instance.ApplyEffect("ShakeL", Vector3.zero);

			RemainingLives--;

			if(RemainingLives > 0){
				_playerController.BecomeInvincible(true, 3f);
			}else{
				if(IsTutorial){
					RemainingLives++;
					BeingDestroyed(true);
				}else{
					BeingDestroyed();
				}
				
			}		

			return true;
		}
		return false;
	}
	private void BeingDestroyed(bool willRevive = false){
		EffectManager.Instance.ApplyEffect("Explosion", PlayerLocalPosContainer.transform.position);

		Player3DModel.SetActive(false);
		CanOperate = false;
		
		if(!willRevive){
			StartCoroutine(GameManager.Instance.GameEnd(false));
		}else{
			StartCoroutine(RollBackFromDeath());
		}
	}
	private IEnumerator RollBackFromDeath(){
		IsStageStarted = false;
		PlayerWorldPosContainer.transform.position = PlayerWorldPosContainer.transform.position - new Vector3(0, 0, 15);
		yield return new WaitForSeconds(1f);
		CanOperate = true;
		IsStageStarted = true;
		Player3DModel.SetActive(true);
	}

	public void GetCoin(Vector3 worldPos, int howMuch, bool isPickedUp){
		var coinTime = Time.timeSinceLevelLoad;

		//拾ったコインだった場合、
		if(isPickedUp){
			if(coinTime == LastPickedUpCoinTime){
				AccumulatedPickedUpCoin += howMuch;
			}else{
				StartCoroutine(WaitForPickingUpCoin(worldPos, howMuch, isPickedUp));
			}
			LastPickedUpCoinTime = coinTime;

			_gameData.TotalPickedUpCoinCount += howMuch;

		}else{
			if(coinTime == LastEnemyCoinTime){
				AccumulatedEnemyCoin += howMuch;
			}else{
				StartCoroutine(WaitForPickingUpCoin(worldPos, howMuch, isPickedUp));
			}
			LastEnemyCoinTime = coinTime;

			_gameData.TotalDroppedCoinCount += howMuch;
		}
	}

	//コインを取得する際、Time.deltaTime秒だけ待機することで、同時に取得したコインをまとめて表示する
	private IEnumerator WaitForPickingUpCoin(Vector3 worldPos, int howMuch, bool isPickedUp){
		yield return new WaitForSeconds(Time.deltaTime);

		//拾ったコインの場合、同時に拾ったコインがあるかもしれないので、それを加算する
		if(isPickedUp){
			howMuch += AccumulatedPickedUpCoin;
			AccumulatedPickedUpCoin = 0;
		}else{
			howMuch += AccumulatedEnemyCoin;
			AccumulatedEnemyCoin = 0;
		}

		if(howMuch > 0){
			howMuch = howMuch + (int)(howMuch * TresureHunter * 0.1f);
		}

		Text theText = PoppingUpTexts[PoppingUpTextCount];

		//表示位置の指定がない場合、操作キャラクターの位置に表示する
		if(worldPos == Vector3.zero)
			worldPos = PlayerLocalPosContainer.transform.localPosition;
			
		theText.rectTransform.position = RectTransformUtility.WorldToScreenPoint (Camera.main, worldPos);
		theText.SendMessage("PopUp", howMuch);

		ModifyCoinAmount(howMuch);

		if(isPickedUp)
			EffectManager.Instance.ApplyEffect("CoinPickUp", worldPos);

		PoppingUpTextCount++;
		if(PoppingUpTextCount >= PoppingUpTexts.Length)
			PoppingUpTextCount = 0;
	}

	//内部的なコイン取得量を変更する
	public bool ModifyCoinAmount(int howMuch, bool save = false, bool view = true){
		if(howMuch >= 0){
			_gameData.CurrentKeepingCoin += howMuch;
			
			if(view)
				MoneyImage.GetComponent<Animator>().SetTrigger("getMoney");
			

		}else{
			if(_gameData.CurrentKeepingCoin + howMuch >= 0){
				_gameData.CurrentKeepingCoin += howMuch;
			}else{
				return false;
			}

		}
		if(view)
			MoneyText.text = _gameData.CurrentKeepingCoin.ToString();

		if(save)
			_gameData.Save();

		return true;
	}


	//指が離れた時に一度呼ばれる
	//指の移動距離によって、フリックかタップかを判断して、対応するメソッドを呼び出す
	public void CallAppropriateTouchOperation(Vector3 beginPos, Vector3 endPos){
		if(IsLevelFinished) SceneManager.LoadScene("Runner");
		if(CanOperate == false) return;

		float moveValueX = endPos.x - beginPos.x;
		float moveValueY = endPos.y - beginPos.y;
		float hypotenuse = Mathf.Sqrt(moveValueX * moveValueX + moveValueY * moveValueY);

		if(hypotenuse > 30){
			_playerController.Flick(new Vector3(moveValueX/hypotenuse, moveValueY/hypotenuse, 0));			
			_gameData.TotalFlickCount++;

		}else{
			if(IsStageStarted)
				ChangeDimensions(0);
		}
	}

	public void ChangeOption(int optionNum, float optionValue){
		switch(optionNum){
			case 0://BGM
				_gameOptionInstance.VolumeOfBGM = optionValue;
				break;
			case 1://SE
				_gameOptionInstance.VolumeOfSE = optionValue;
				break;
			case 2://Flick
				_gameOptionInstance.FlickOffset = optionValue;
				break;
			case 3://DimensionBlocks
				_gameOptionInstance.IsEnabledDimensionBlocks = !_gameOptionInstance.IsEnabledDimensionBlocks;
				break;
			case 4://HorizontalWires
				_gameOptionInstance.IsEnabledHorizontalWires = !_gameOptionInstance.IsEnabledHorizontalWires;
				break;
			case 5://LightWeightMode
				_gameOptionInstance.IsLightWeightMode = !_gameOptionInstance.IsLightWeightMode;
				break;
		}
	}
}