﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// アタッチしたオブジェクトを回転させる
/// </summary>
public class PlanetRotater : MonoBehaviour {
	[SerializeField] private float RotateSpeedX;
	[SerializeField] private float RotateSpeedY;
	[SerializeField] private float RotateSpeedZ;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(new Vector3(RotateSpeedX, RotateSpeedY, RotateSpeedZ) * Time.deltaTime);
	}
}
