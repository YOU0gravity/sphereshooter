﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageData {
	public readonly static StageData Instance = new StageData();
	public int StageLength { get; set; }
	public int CoinAppearancePercentage { get; set; }
	public int CoinPlaceInSameLinePercentage { get; set; }
	public float EnemyAppearancePercentage { get; set; }
	public int[] AppearEnemiesArray { get; set; }
	public float SpeedUpCoef { get; set; }
	public int[,] RoadsInfo { get; set; }

	public void CreateRoadsInfo(){
		//デフォルトの設定
		AppearEnemiesArray = new int[]{1,1,1,1,1,2,2,3,3};
		CoinAppearancePercentage = 30;
		CoinPlaceInSameLinePercentage = 65;
		EnemyAppearancePercentage = 10;
		StageLength = 200;
		SpeedUpCoef = 0.2f;
		CreateRoadsInfoRandomly();
	}

	public void CreateRoadsInfoRandomly(){
		this.RoadsInfo = this.StageGenerator(this.StageLength);
	}

	private int[,] StageGenerator(int howManyBoxes){
		int[,] generatedBoxes = new int[howManyBoxes,27];
		int[] coinAppearancePercentage = new int[9];


		float tempEnemyAppearancePercentage = EnemyAppearancePercentage;
		for(int box_num=0; box_num<howManyBoxes; box_num++){
			EnemyAppearancePercentage += 0.2f;
			if(EnemyAppearancePercentage > tempEnemyAppearancePercentage * 1.2f){
				tempEnemyAppearancePercentage *= 1.1f; 
				EnemyAppearancePercentage = tempEnemyAppearancePercentage;
			}

			/*
			*	Boxは(z:3 * y:3 * x:3 = 27)で構成される
			*	Panelは(y:3 * x:3 = 9)で構成される
			*	3Panel = 1Boxとなる
			*/
			int[] oneRoad = new int[27];
			
			int cellIndexOfBox = 0;
			for(int z=0; z<3; z++){
				int[] onePanel = new int[9];
				for(int y=0; y<3; y++){
					for(int x=0; x<3; x++){
						int cellIndexOfPanel = cellIndexOfBox-(z*9);
						int cell = 0;
						if(Random.Range(0,100) < coinAppearancePercentage[cellIndexOfPanel]){
							cell = 100;
						}


						
						//敵を生成する(1~99)(コインと共存可能)
						if(Random.Range(0f,100f) < this.EnemyAppearancePercentage){
							int appearIndex = Random.Range(0, this.AppearEnemiesArray.Length);
							cell += this.AppearEnemiesArray[appearIndex];

							//低確率で金の敵を生成
							if(Random.Range(0,250) < 1){
								cell = 99;
							}
						}
								
						oneRoad[cellIndexOfBox] = cell;
						onePanel[cellIndexOfPanel] = cell;
						cellIndexOfBox++;
					}
				}
				coinAppearancePercentage = this.CoinAppearanceForecaster(onePanel);
			}

			string debugstr = "";
			for(int i=0;i<27;i++){
				generatedBoxes[box_num, i] = oneRoad[i];
				debugstr += oneRoad[i] + ",";
			}
			// Debug.Log(box_num + ": " + debugstr);
			
		}

		return generatedBoxes;
	}
	private int[] CoinAppearanceForecaster(int[] onePanel){
		int[] percentagePanel = new int[9];
		int highRate = this.CoinAppearancePercentage;
		int middleRate = highRate / 2;
		int lowRate = highRate / 4;
		for(int i=0; i<9; i++){
			if(onePanel[i] == 100){
				percentagePanel[i] = highRate;

				if(i >= 3){//上端でなければ、一つ上のマスに確率プラス
					percentagePanel[i-3] += middleRate;
				}

				if(i%3 != 0){//左端でなければ、一つ左のマスに確率プラス
					percentagePanel[i-1] += middleRate;
				}

				if(i%3 != 2){//右端でなければ、一つ右のマスに確率プラス
					percentagePanel[i+1] += middleRate;
				}

				if(i < 6){//下端でなければ、一つしたのマスに確率プラス
					percentagePanel[i+3] += middleRate;
				}
			}
		}
		string debugstr = "";
		for(int i=0; i<9; i++){
			if(percentagePanel[i] == 0)
				percentagePanel[i] = lowRate;

			debugstr += percentagePanel[i] + ", ";
		}
		// Debug.Log("パーセンテージ: " + debugstr);

		return percentagePanel;
	}

	private int[] ColumnGenerator(int[] beforeCol, int[] beforeCol2, int generateItemNum, int appearPercentage, int sameLinePercentage){
		int[] generatedColumn = new int[3];
		int soulIndexer = -1;
		int dice100 = Random.Range(0,100);
		
		if(dice100 < appearPercentage){
			for(int i=0;i<3;i++){
				if(beforeCol[i] == generateItemNum){
					soulIndexer = i;
					break;
				}
			}

			if(soulIndexer == -1){//1つ前の行にソウルが見つからなかった場合
				//2つ前を調べる
				for(int i=0;i<3;i++){
					if(beforeCol2[i] == generateItemNum){
						soulIndexer = i;
						break;
					}
				}
				if(soulIndexer == -1){//1つ前、2つ前の行ともにソウルが見つからなかった場合
					soulIndexer = Random.Range(0,3);
				}else{//1つ前にはソウルがなく、2つ前にはソウルが見つかったとき
					dice100 = Random.Range(0, 100);
					if(dice100 < sameLinePercentage){
						//同じ列へ置く(soulIndexerの変更なし)
					}else{
						switch(soulIndexer){
							case 0:
								soulIndexer = 1;
								break;
							case 1:
								if(dice100 % 2 == 0){
									soulIndexer = 0;
								}else{
									soulIndexer = 2;
								}
								break;
							case 2:
								soulIndexer = 1;
								break;
						}
					}
				}

			}else{//1つ前の行にソウルが見つかった場合
				dice100 = Random.Range(0, 100);
				if(dice100 < sameLinePercentage){
					//同じ列へおく(soulIndexerの変更なし)
				}else{
					//違う列へおく
					switch(soulIndexer){//2つの列から、1つを絞り込む
						case 0:
							soulIndexer = 1;
							break;
						case 1:
							if(dice100 % 2 == 0){
								soulIndexer = 0;
							}else{
								soulIndexer = 2;
							}
							break;
						case 2:
							soulIndexer = 1;
							break;
					}
				}
			}
		}

		if(soulIndexer != -1 && generatedColumn[soulIndexer] == 0)
				generatedColumn[soulIndexer] = generateItemNum;
		
		// string result = "";
		// for(int i=0; i<3; i++){
		// 	result += generatedColumn[i].ToString() + ",";
		// }
		// Debug.Log(result);
		return generatedColumn;
	}
}
