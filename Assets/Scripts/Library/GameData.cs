﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData {
	public int ApplicationStartUpCount { get; set; }
	public int SelectedCharaID { get; set; }
	public int TotalDestroyedEnemyCount { get; set; }
	public int TotalPickedUpCoinCount { get; set; }
	public int TotalDroppedCoinCount { get; set; }
	public int MaxStageLevel { get; set; }
	public int MaxDestroyPoint { get; set; }
	public int TotalFlickCount { get; set; }
	public int TotalShootCount { get; set; }
	public int CurrentKeepingCoin { get; set; }
	public int RouletteCount { get; set; }

	public GameData(){
		this.ApplicationStartUpCount = PlayerPrefs.GetInt("StartUpCount", 0);
		this.SelectedCharaID = PlayerPrefs.GetInt("SelectedCharaID", 0);
		this.TotalDestroyedEnemyCount = PlayerPrefs.GetInt("DestroyedEnemyCount", 0);
		this.TotalPickedUpCoinCount = PlayerPrefs.GetInt("PickedUpCoinCount", 0);
		this.TotalDroppedCoinCount = PlayerPrefs.GetInt("DroppedCoinCount", 0);
		this.MaxStageLevel = PlayerPrefs.GetInt("MaxStageLevel", 0);
		this.MaxDestroyPoint = PlayerPrefs.GetInt("MaxDestroyPoint", 0);
		this.TotalFlickCount = PlayerPrefs.GetInt("FlickCount", 0);
		this.TotalShootCount = PlayerPrefs.GetInt("ShootCount", 0);
		this.CurrentKeepingCoin = PlayerPrefs.GetInt("CurrentKeepingCoin", 0);
		this.RouletteCount = PlayerPrefs.GetInt("RouletteCount", 0);
	}
	public void Save(){
		PlayerPrefs.SetInt("StartUpCount", this.ApplicationStartUpCount);
		PlayerPrefs.SetInt("SelectedCharaID", this.SelectedCharaID);
		PlayerPrefs.SetInt("DestroyedEnemyCount", this.TotalDestroyedEnemyCount);
		PlayerPrefs.SetInt("PickedUpCoinCount", this.TotalPickedUpCoinCount);
		PlayerPrefs.SetInt("DroppedCoinCount", this.TotalDroppedCoinCount);
		PlayerPrefs.SetInt("MaxStageLevel", this.MaxStageLevel);
		PlayerPrefs.SetInt("MaxDestroyPoint", this.MaxDestroyPoint);
		PlayerPrefs.SetInt("FlickCount", this.TotalFlickCount);
		PlayerPrefs.SetInt("ShootCount", this.TotalShootCount);
		PlayerPrefs.SetInt("CurrentKeepingCoin", this.CurrentKeepingCoin);
		PlayerPrefs.SetInt("RouletteCount", this.RouletteCount);
	}
	public void SaveChara(){
		PlayerPrefs.SetInt("SelectedCharaID", this.SelectedCharaID);
	}

	public void Reset(){
		this.ApplicationStartUpCount = 0;
		this.SelectedCharaID = 0;
		this.TotalDestroyedEnemyCount = 0;
		this.TotalPickedUpCoinCount = 0;
		this.TotalDroppedCoinCount = 0;
		this.MaxStageLevel = 0;
		this.MaxDestroyPoint = 0;
		this.TotalFlickCount = 0;
		this.TotalShootCount = 0;
		this.CurrentKeepingCoin = 0;
		this.RouletteCount = 0;

		this.Save();
		Debug.Log("GameDataをリセットしました");
	}

	public string InspectPropaties(){
		var sb = new System.Text.StringBuilder();
		sb.Append("EnemyCount: ");
		sb.Append(this.TotalDestroyedEnemyCount);
		sb.AppendLine();
		sb.Append("PickedUp: ");
		sb.Append(this.TotalPickedUpCoinCount);
		sb.AppendLine();
		sb.Append("Dropped: ");
		sb.Append(this.TotalDroppedCoinCount);
		sb.AppendLine();
		sb.Append("MaxStageLevel: ");
		sb.Append(this.MaxStageLevel);
		sb.AppendLine();
		sb.Append("MaxDestroyPoint: ");
		sb.Append(this.MaxDestroyPoint);
		sb.AppendLine();
		sb.Append("Flick: ");
		sb.Append(this.TotalFlickCount);
		sb.AppendLine();
		sb.Append("Shoot: ");
		sb.Append(this.TotalShootCount);
		sb.AppendLine();

		return sb.ToString();
	}
}