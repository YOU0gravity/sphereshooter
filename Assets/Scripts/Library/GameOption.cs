﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOption {
	public float VolumeOfBGM { get; set; }
	public float VolumeOfSE { get; set; }
	public float FlickOffset { get; set; }
	public bool IsEnabledDimensionBlocks { get; set; }
	public bool IsEnabledHorizontalWires { get; set; }
	public bool IsLightWeightMode { get; set; }

	public GameOption(){
		VolumeOfBGM = PlayerPrefs.GetFloat("VolumeOfBGM", 1);
		VolumeOfSE = PlayerPrefs.GetFloat("VolumeOfSE", 1);
		FlickOffset = PlayerPrefs.GetFloat("FlickOffset", 30);
		IsEnabledDimensionBlocks = PlayerPrefs.GetInt("DimensionBlocks", 1) > 0 ? true : false; 
		IsEnabledHorizontalWires = PlayerPrefs.GetInt("HorizontalWires", 1) > 0 ? true : false;
		IsLightWeightMode = PlayerPrefs.GetInt("HorizontalWires", 1) > 0 ? true : false;
	}

	public void Save(){
		PlayerPrefs.SetFloat("VolumeOfBGM", VolumeOfBGM);
		PlayerPrefs.SetFloat("VolumeOfSE", VolumeOfSE);
		PlayerPrefs.SetFloat("FlickOffset", FlickOffset);
		PlayerPrefs.SetInt("DimensionBlocks", IsEnabledDimensionBlocks ? 1 : 0); 
		PlayerPrefs.SetInt("HorizontalWires", IsEnabledHorizontalWires ? 1 : 0);
		PlayerPrefs.SetInt("HorizontalWires", IsLightWeightMode ? 1 : 0);
	}
}