﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterLockManager { 
	public int[] UnlockedNumber;
	public string UnlockedNumberString;
	private int MaxCharacterNumber;
	public CharacterLockManager (){
		this.MaxCharacterNumber = 30;
		this.UnlockedNumber = new int[this.MaxCharacterNumber];
		this.Load();
	}

	public void Save(){
		System.Text.StringBuilder sb = new System.Text.StringBuilder();
		for(int i=0; i<this.MaxCharacterNumber; i++){
			sb.Append(this.UnlockedNumber[i]);
			sb.Append(',');
		}
		this.UnlockedNumberString = sb.ToString();

		PlayerPrefs.SetString("UnlockedNumber", this.UnlockedNumberString);
		PlayerPrefs.Save();
	}

	public void Load(){
		this.UnlockedNumberString = PlayerPrefs.GetString("UnlockedNumber", "");
		// Debug.Log("UnlockedNumber: /" + this.UnlockedNumberString + "/");
		if(!string.IsNullOrEmpty(this.UnlockedNumberString)){
			string[] str = this.UnlockedNumberString.Split(',');
			for(int i=0; i<str.Length-1; i++){
				// Debug.Log(i + ": " + str[i]);
				int parsedNumber = 0;
				int.TryParse(str[i], out parsedNumber);
				this.UnlockedNumber[i] = parsedNumber;
			}

			if(str.Length-1 < this.MaxCharacterNumber){//キャラ格納領域を新たに追加したとき、これが呼ばれる
				for(int i=str.Length-1; i<this.MaxCharacterNumber; i++){
					this.UnlockedNumber[i] = 0;
				}
				this.Save();
			}
		}
	}

	public void Reset(){
		for(int i=0; i<this.UnlockedNumber.Length; i++){
			this.UnlockedNumber[i] = 0;
		}
		Debug.Log("キャラ解禁状況をリセットしました");
		this.Save();
	}
}