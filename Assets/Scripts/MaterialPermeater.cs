﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// プレイヤーと違う高さにある障害物などを、透明にするクラス
/// 現在使用していない
/// </summary>
public class MaterialPermeater : MonoBehaviour {
	[SerializeField] private Material[] Mats;
	[SerializeField] private float Offset;
	private Renderer Rend;
	private int CurrentMatNum;
	private GameObject TargetObject;

	// Use this for initialization
	void Start () {
		this.TargetObject = GameManager.PlayerLocalPosContainer;
		this.Rend = GetComponent<Renderer>();
		this.CurrentMatNum = 0;
	}
	

	void Update(){
		if(GameManager.DimensionNumber == 0){
			float absOffsetY = Mathf.Abs(transform.position.y - this.TargetObject.transform.position.y - Offset);
			
			if(absOffsetY >= 3){
				if(this.CurrentMatNum != 2){
					this.Rend.material = this.Mats[2];
					this.CurrentMatNum = 2;
				}
				return;
			}else{
				if(this.CurrentMatNum != 1){
					this.Rend.material = this.Mats[1];
					this.CurrentMatNum = 1;
				}
			}
		}else{
			if(this.CurrentMatNum != 0){
				this.Rend.material = this.Mats[0];
				this.CurrentMatNum = 0;
			}
		}
		

	}

}
